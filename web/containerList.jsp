<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="com.kujaza.connection.Database"%>
<!DOCTYPE html>
<html>
    <head>
        <%@ include file="lang.jsp" %>
        <title><%=tr[lang][144]%> &bull; KUJAZA</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta charset="utf-8" />
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
        <link href="assets/css/main.css" rel="stylesheet" />
        <link href="assets/css/animate.css" rel="stylesheet" />
        <link href="assets/css/aos.css" rel="stylesheet" />
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
        <link href="assets/css/tabs.css" rel="stylesheet" />
        <link href="assets/css/tabstyles.css" rel="stylesheet" />
        <link href="assets/css/DatePicker.css" rel="stylesheet" />
    </head>
    <body class="bodybg">
        <%
            Database db = new Database();
            db.connect();

            if (session.getAttribute("email") == null) {
                response.sendRedirect("login.jsp");
            }
            
//            if(!session.getAttribute("service").equals("shipper")) {
//                response.sendRedirect("index.jsp");
//            }

            try {

                Statement st0 = db.connection.createStatement();
                Statement st1 = db.connection.createStatement();
                Statement st2 = db.connection.createStatement();
                String q0 = "";
                String q1 = "";
                String q2 = "";
                ResultSet rs0;
                ResultSet rs1;
                ResultSet rs2;
                
                if(request.getParameter("deleteId")!=null && !request.getParameter("deleteId").equals("")) {
                    q2="delete from container where id="+request.getParameter("deleteId");
                    st2.executeUpdate(q2);
                }


        %>
        <div>
        <!-- HEADER -->
        <%@ include file="header2.jsp" %>
        <!-- END HEADER -->


        <%            
            
            q0="select * from shipper where email='"+session.getAttribute("email").toString()+"'";
            
            rs0=st0.executeQuery(q0);
            rs0.next();
            
            
            
            
            q1 = "select * from container where shipper_id="+rs0.getString("id");
            rs1=st1.executeQuery(q1);
            

        %>

        <div class="srHeader container">
            <h1 class="srTitle" style="font-size: 30px;"><b><%=tr[lang][79]%></b></h1>

        <%--    <div class="srFilters row">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                    <span class="fixInfo">Departure from: - <%=request.getParameter("departure")%></span>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                    <span class="fixInfo">Arrival to: - <%=request.getParameter("arrival")%></span>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                    <span class="fixInfo">Volume: -</span>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                    <span class="fixInfo">Ship date: - <%=request.getParameter("shippingDate")%></span>
                </div>
            </div>
        --%>
        <%--<h3 class="srTitle" style="font-size: 20px;"><b><%=tr[lang][146]%> <%=rowcount%> <%=tr[lang][147]%></b></h3>--%>  
        </div>


        <div class="container" id="SearchResults">
            <%

                while (rs1.next()) {


            %>

            <div class="row result" data-aos="fade-right" data-aos-delay="400" data-aos-once="true" data-aos-duration="1200" onclick="location.href = 'containerDescription.jsp?containerId=<%=rs1.getString("id")%>'">
                <div class="col-lg-2 col-md-2 result-img-user col-sm-4 col-xs-12">
                    <img class="img-responsive center-block img-circle" src="assets/img/users/<%=rs0.getString("id")%>.jpg" />
                </div>
                <div class="col-lg-10 col-md-10 col-sm-8 col-xs-12">
                    <h4 class="result-title" style="font-size: 25px;"><b><%=rs1.getString("name")%></b></h4>
                    <p class="ReviewNumber" style="font-size: 12px;"><span class="fa fa-star" ></span><b><span>5.0</span> (1k+)</b></p>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                        <span class="fixInfo"><%=tr[lang][148]%></span>
                        <%=rs1.getString("departure")%>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                        <span class="fixInfo"><%=tr[lang][149]%></span>
                        <%=rs1.getString("arrival")%>
                    </div>

                    <div class="col-lg-2 col-md-3 col-sm-6 col-xs-6">
                        <span class="fixInfo"><%=tr[lang][150]%></span>
                        <%=rs1.getString("shipping_date")%>
                    </div>

                    <div class="col-lg-2 col-md-3 col-sm-6 col-xs-6">
                        <p class="result-price">$<%=rs1.getString("price_per_m3")%> / m<sup>3</sup></p>
                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-6 col-xs-6">
                    <form action="containerList.jsp" method="post">
                        <input type="hidden" name="deleteId" value="<%=rs1.getString("id")%>"/>
                        <button class="btn btn-danger" type="submit"><%=tr[lang][236]%></button>
                    </form>
                    </div>
                        
                </div>
            </div>

            <%

                }
            %>
        </div>

        <!-- MODALS -->
        <div id="SearchForm" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"><%=tr[lang][151]%> <span class="fa fa-search"></span></h4>
                    </div>
                    <form method="get" action="searchResult.jsp">
                    <div class="modal-body">
                        <h2><%=tr[lang][152]%></h2>
                        <div class="stepwizard col-md-offset-3">
                            <div class="stepwizard-row setup-panel">
                                <div class="stepwizard-step">
                                    <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
                                    <p><%=tr[lang][153]%></p>
                                </div>
<!--                                <div class="stepwizard-step">
                                    <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                                    <p>Step 2</p>
                                </div>-->
                                <div class="stepwizard-step">
                                    <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                                    <p><%=tr[lang][154]%></p>
                                </div>
                            </div>
                        </div>

                        <div class="row setup-content" id="step-1">
                            <span class="input input--kozakura">
                                <input class="input__field input__field--kozakura" name="departure" type="text" id="input-7" />
                                <label class="input__label input__label--kozakura" for="input-7">
                                    <span class="input__label-content input__label-content--kozakura" data-content="Departure from"><%=tr[lang][155]%></span>
                                </label>
                                <svg class="graphic graphic--kozakura" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                                <path d="M1200,9c0,0-305.005,0-401.001,0C733,9,675.327,4.969,598,4.969C514.994,4.969,449.336,9,400.333,9C299.666,9,0,9,0,9v43c0,0,299.666,0,400.333,0c49.002,0,114.66,3.484,197.667,3.484c77.327,0,135-3.484,200.999-3.484C894.995,52,1200,52,1200,52V9z"/>
                                </svg>
                            </span>

                            <span class="input input--kozakura">
                                <input class="input__field input__field--kozakura" name="arrival" type="text" id="input-7" />
                                <label class="input__label input__label--kozakura" for="input-7">
                                    <span class="input__label-content input__label-content--kozakura" data-content="Arrival to"><%=tr[lang][156]%></span>
                                </label>
                                <svg class="graphic graphic--kozakura" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                                <path d="M1200,9c0,0-305.005,0-401.001,0C733,9,675.327,4.969,598,4.969C514.994,4.969,449.336,9,400.333,9C299.666,9,0,9,0,9v43c0,0,299.666,0,400.333,0c49.002,0,114.66,3.484,197.667,3.484c77.327,0,135-3.484,200.999-3.484C894.995,52,1200,52,1200,52V9z"/>
                                </svg>
                            </span>
                            <button class="aksBtn nextBtn" type="button" ><span class="fa fa-arrow-right"></span></button>
                        </div>
<!--                        <div class="row setup-content" id="step-2">
                            <select name="" id="cusSelectbox">
                                <option value="Cubic">Cubic</option>
                                <option value="Cylindrical">Cylindrical</option>
                                <option value="Car">Car</option>
                            </select>

                            <span class="input input--kozakura">
                                <input class="input__field input__field--kozakura" type="text" id="input-7" />
                                <label class="input__label input__label--kozakura" for="input-7">
                                    <span class="input__label-content input__label-content--kozakura" data-content="Size">Size</span>
                                </label>
                                <svg class="graphic graphic--kozakura" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                                <path d="M1200,9c0,0-305.005,0-401.001,0C733,9,675.327,4.969,598,4.969C514.994,4.969,449.336,9,400.333,9C299.666,9,0,9,0,9v43c0,0,299.666,0,400.333,0c49.002,0,114.66,3.484,197.667,3.484c77.327,0,135-3.484,200.999-3.484C894.995,52,1200,52,1200,52V9z"/>
                                </svg>
                            </span>
                            <button class="aksBtn nextBtn" type="button" ><span class="fa fa-arrow-right"></span></button>
                        </div>-->
                        <div class="row setup-content" id="step-2">
                            <div class="col-xs-6 col-md-offset-3">
                                <div class="col-md-12">
                                    <span class="input input--kozakura">
                                                            <input type="text" class="form-control" name="shippingDate" id="txtShippingDate" placeholder="Shipping Date" />
                                                            
                                                    </span
                                    <input type="text" placeholder="Ship Date" id="theInput" />
                                    <button class="aksBtn" type="submit"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Scripts -->
        <script src="assets/js/jquery-3.1.1.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/classie.js"></script>
        <script src="assets/js/KeepOpen.js"></script>
        <script src="assets/js/typed.min.js"></script>
        <script src="assets/js/aos.js"></script>
        <script>AOS.init();</script>
        <script src="assets/js/main.js"></script>
        <script src="assets/js/DatePicker.js"></script>
        <script src="assets/js/cbpFWTabs.js"></script>
        <script>
                (function () {

                    [].slice.call(document.querySelectorAll('.tabs')).forEach(function (el) {
                        new CBPFWTabs(el);
                    });

                })();

                $('.acc-type').click(function () {
                    $('.acc-type').removeClass('this-type');
                    $('.acc-type-chkd').removeAttr('checked');
                    $(this).addClass('this-type');
                    $(this).next('.acc-type-chkd').attr('checked', 'checked');
                });
        </script>
    </div>

        <%
            } catch (Exception e) {
                System.out.println(e);
            } finally {
                db.close();
            }
        %>
    </body>
</html>
