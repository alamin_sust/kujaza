<%@page import="java.sql.ResultSet"%>
<%@page import="com.kujaza.connection.Database"%>
<%@page import="java.sql.Statement"%>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">
        <%@ include file="lang.jsp" %>
        <title><%=tr[lang][179]%> &bull; Kujaza &bull; <%=tr[lang][180]%></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta charset="utf-8" />
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
        <link href="assets/css/main.css" rel="stylesheet" />
        <link href="assets/css/animate.css" rel="stylesheet" />
        <link href="assets/css/aos.css" rel="stylesheet" />
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
        <link href="assets/css/DatePicker.css" rel="stylesheet" />

        <style>
            .checked {
                color: orange;
            }
        </style>
    </head>
    <body class="bodybg">

        <%
            Database db = new Database();
            db.connect();

            //try {

                if (session.getAttribute("email") == null || session.getAttribute("email").toString().equals("")) {
                    response.sendRedirect("index.jsp");
                }

                String shipperId = request.getParameter("sId");

                Statement st0 = db.connection.createStatement();
                Statement st1 = db.connection.createStatement();
                Statement st2 = db.connection.createStatement();
                Statement st3 = db.connection.createStatement();
                Statement st4 = db.connection.createStatement();
                Statement st5 = db.connection.createStatement();
                String q0 = "";
                String q1 = "";
                String q2 = "";
                String q3 = "";
                String q4 = "";
                String q5 = "";
                
                ResultSet rs0;
                ResultSet rs1;
                ResultSet rs2;
                ResultSet rs3;
                ResultSet rs4;
                ResultSet rs5;
                
                q2 = "select * from customer where email='" + session.getAttribute("email").toString() + "'";
                rs2 = st2.executeQuery(q2);
                rs2.next();

                q3 = "select count(*) as cnt,sum(star) as starByMe from rating where customer_id=" + rs2.getString("id")+" and shipper_id="+shipperId;
                rs3 = st3.executeQuery(q3);
                rs3.next();
                String ratingByMe = "";
                if (request.getParameter("star") != null && !request.getParameter("star").equals("")) {
                    ratingByMe=request.getParameter("star");
                    if(rs3.getString("cnt").equals("1")) {
                        q4="update rating set star="+request.getParameter("star")+", message='"+request.getParameter("message")+"' where customer_id="+ rs2.getString("id")+" and shipper_id="+shipperId;
                    } else {
                        q0="select max(id)+1 as mxId from rating";
                        rs0=st0.executeQuery(q0);
                        
                        rs0.next();
                        q4 = "insert into rating (id,star,message,customer_id,shipper_id) values ("+rs0.getString("mxId")+","+request.getParameter("star")+",'"+request.getParameter("message")+"',"+rs2.getString("id")+","+shipperId+")";
                    }
                    st4.executeUpdate(q4);
                } else {
                    if(rs3.getString("cnt").equals("1")) {
                    ratingByMe = rs3.getString("starByMe");
                            }
                }

                q1 = "select * from shipper where id=" + shipperId;

                rs1 = st1.executeQuery(q1);

                String sId = "";
                String sName = "";
                String sImgUrl = "";
                String sRatingCount = "";
                String sTotalRatingPoint = "";
                String sOrderCount = "";
                String sMemberSince = "";
                String sAboutMe = "";
                double sRating = 0.0;

                while (rs1.next()) {
                    sId = rs1.getString("id");
                    sName = rs1.getString("name");
                    sImgUrl = rs1.getString("img_url");
                    sRatingCount = rs1.getString("rating_count");
                    sTotalRatingPoint = rs1.getString("total_rating_point");
                    sOrderCount = rs1.getString("order_count");
                    sMemberSince = rs1.getString("member_since");
                    sAboutMe = rs1.getString("about_me");
                    if (Double.parseDouble(sRatingCount) != 0.0) {
                        sRating = Double.parseDouble(sTotalRatingPoint) / Double.parseDouble(sRatingCount);
                    }

                }
                
                q5="select count(*) as cnt,avg(star) as avgRating from rating where shipper_id="+shipperId;
                
               
                rs5=st5.executeQuery(q5);
                rs5.next();
                
                sRating=rs5.getInt("cnt")>=1?Double.parseDouble(rs5.getString("avgRating")):0;
                sRatingCount=rs5.getString("cnt");


        %>


        <div>

            <!-- HEADER -->
            <%@ include file="header2.jsp" %>
            <!-- END HEADER -->

            <div class="container-fluid martop7">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <div class="sellerInfo animated slideInLeft">
                            <p class="sellerInfoTitle"><%=tr[lang][181]%></p>
                            <hr />
                            <i class="martop7"></i>
                            <img class="img-responsive img-circle center-block" src="assets/img/users/<%=sId%>.jpg" />
                            <div class="row">
                                <label id="lblSellerName" class="center-block"><%=sName%></label>
                                <p class="ReviewNumber">
                                    <%--<h2>Star Rating</h2>--%>
                                    <%if (sRating >= 0.5) {%><span class="fa fa-star checked"></span><%}%>
                                    <%if (sRating >= 1.5) {%><span class="fa fa-star checked"></span><%}%>
                                    <%if (sRating >= 2.5) {%><span class="fa fa-star checked"></span><%}%>
                                    <%if (sRating >= 3.5) {%><span class="fa fa-star checked"></span><%}%>
                                    <%if (sRating >= 4.5) {%><span class="fa fa-star checked"></span><%}%>
                                    

                                    <span> <%=sRating%></span> (<%=sRatingCount%>)</p>
                            </div>
                                    <%if(!rs2.getString("id").equals(shipperId)) {%>
                            <div class="row">
                                
                                <%=tr[lang][238]%>: <%=ratingByMe%>
                                
                                <form method="post" action="shipperProfile.jsp?sId=<%=sId%>">
                                    <input type="radio" name="star" value="1"><%=tr[lang][239]%> -</input>
                                    <input type="radio" name="star" value="2"><%=tr[lang][240]%> -</input>
                                    <input type="radio" name="star" value="3"><%=tr[lang][241]%> -</input>
                                    <input type="radio" name="star" value="4"><%=tr[lang][242]%> -</input>
                                    <input type="radio" name="star" value="5"><%=tr[lang][243]%></input>
                                    <br>
                                        <input type="text" name="message" placeholder="<%=tr[lang][244]%>" required=""></input>
                                        <button type="submit" class="btn btn-primary"><%=tr[lang][237]%></button>
                                </form>

                            </div>
                                    <%}%>
                            <div class="row">
                                <label class="aksLbl"><%=tr[lang][182]%> <span id="lblOrderCountGig"> <%=sOrderCount%></span></label>
                            </div>
                            <div class="row">
                                <label class="aksLbl"><%=tr[lang][183]%> <span id="lblMemberDate"> <%=sMemberSince%></span></label>
                            </div>
                            <div class="row form-inline">
                                <button class="aksBtn" onclick="location.href = 'conversation.jsp?idOther=<%=sId%>'"><%=tr[lang][184]%> <span class="fa fa-envelope"></span></button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                        <div class="row">
                            <div class="aksBox">    
                                <h2><%=tr[lang][185]%></h2>
                                <hr />
                                <p class="Description">
                                    <%=sAboutMe%>
                                </p>
                            </div>
                        </div>
                        <div class="row martop2">
                            <div class="aksBox SellerReviews">
                                <h2><%=tr[lang][186]%></h2>
                                <hr />
                                <%
                                    q2 = "select * from rating where shipper_id=" + shipperId;
                                    rs2 = st2.executeQuery(q2);
                                    while (rs2.next()) {
                                        String cId = rs2.getString("customer_id");

                                        q3 = "select * from customer where id=" + cId;
                                        rs3 = st3.executeQuery(q3);
                                        rs3.next();


                                %>

                                <div class="row review">
                                    <div class="col-lg-2 col-md-2 col-sm-3 col-xs-4">
                                        <img class="img-responsive img-circle center-block" src="assets/img/users/<%=cId%>.jpg" />
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-9 col-xs-8">
                                        <div class="row">
                                            <label class="BuyerName"><%=rs3.getString("name")%></label>
                                        </div>
                                        <div class="row">
                                            <span class="fa fa-star"></span> <%=rs2.getString("star")%>
                                        </div>
                                        <div class="row martop2">
                                            <%=rs2.getString("message")%>
                                        </div>
                                    </div>
                                </div>

                                <%

                                    }

                                %>
                            </div></div>
                    </div>
                </div>
            </div>

            <!-- Scripts -->
            <script src="assets/js/jquery-3.1.1.min.js"></script>
            <script src="assets/js/bootstrap.min.js"></script>
            <script src="assets/js/classie.js"></script>
            <script src="assets/js/KeepOpen.js"></script>
            <script src="assets/js/typed.min.js"></script>
            <script src="assets/js/aos.js"></script>
            <script>AOS.init();</script>
            <script src="assets/js/main.js"></script>
            <script src="assets/js/DatePicker.js"></script>
        </div>
        <% //                            } catch (Exception e) {
            //    System.out.println(e);
          //  } finally {
          //      db.close();
          //  }
        %>
    </body>
</html>