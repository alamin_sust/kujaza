-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: transport
-- ------------------------------------------------------
-- Server version	5.5.60-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `conversation`
--

DROP TABLE IF EXISTS `conversation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conversation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender_id` int(11) DEFAULT NULL,
  `sender_type` varchar(110) DEFAULT NULL,
  `receiver_id` int(11) DEFAULT NULL,
  `receiver_type` varchar(110) DEFAULT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `message` varchar(1010) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conversation`
--

LOCK TABLES `conversation` WRITE;
/*!40000 ALTER TABLE `conversation` DISABLE KEYS */;
INSERT INTO `conversation` VALUES (1,1,'shipper',1,'customer','2017-08-19 22:52:10','Hello Customer 1'),(2,1,'customer',1,'shipper','2017-08-19 22:52:10','Hello Shipper 1'),(3,2,'shipper',1,'customer','2017-08-19 22:52:10','Hello MAN!'),(4,1,'shipper',2,'customer','2017-08-19 22:53:16','hello customer 2'),(5,1,'shipper',1,'customer','2017-08-20 02:12:39','hiii'),(6,6,'shipper',1,'customer','2017-09-09 04:01:35','hiii'),(7,6,'shipper',8,'customer','2017-09-09 04:03:18','helllllloooooo'),(8,8,'customer',6,'shipper','2017-09-09 04:04:13','lol'),(9,7,'shipper',1,'customer','2017-09-13 23:32:55','Hello'),(10,21,'shipper',1,'customer','2017-12-02 03:41:49','hiii'),(11,1,'customer',21,'shipper','2017-12-02 03:45:42','yo'),(12,1,'customer',2,'shipper','2017-12-02 03:46:11','yyyyyy');
/*!40000 ALTER TABLE `conversation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(110) DEFAULT NULL,
  `img_url` varchar(110) DEFAULT NULL,
  `address` varchar(110) DEFAULT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `mobile` varchar(30) DEFAULT NULL,
  `password` varchar(110) NOT NULL,
  `email` varchar(110) NOT NULL,
  `about_me` varchar(1010) DEFAULT NULL,
  `question1` varchar(1010) DEFAULT NULL,
  `answer1` varchar(1010) DEFAULT NULL,
  `question2` varchar(1010) DEFAULT NULL,
  `answer2` varchar(1010) DEFAULT NULL,
  `is_verified` int(11) DEFAULT NULL,
  `veification_key` varchar(110) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer`
--

LOCK TABLES `customer` WRITE;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
INSERT INTO `customer` VALUES (1,'werqwerqwe',NULL,NULL,NULL,NULL,'11','b@b',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,'frqwer',NULL,NULL,NULL,NULL,'22','c@c',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,'',NULL,NULL,NULL,NULL,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(4,'riss',NULL,NULL,NULL,NULL,'gio','rissasmusic@hotmail.fr',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5,'Al Amin',NULL,NULL,NULL,NULL,'11','alamin@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6,'shippersust',NULL,NULL,NULL,NULL,'12','s@sust',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(7,'Sai',NULL,NULL,NULL,NULL,'Riss','hsaidyi@me.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(15,'minar rahman',NULL,NULL,NULL,NULL,'11','minar@rahman',NULL,'what was the first name of your best childhood friend?','aa','who is your icon?','bb',NULL,NULL),(16,'shahad',NULL,NULL,NULL,NULL,'11','ss@ss',NULL,'what was the first name of your best childhood friend?','aa','who is your icon?','bb',NULL,NULL),(17,'dummy1',NULL,NULL,NULL,NULL,'11','aaa',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(18,'dummy2',NULL,NULL,NULL,NULL,'11','aaaa',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(19,'alamin',NULL,NULL,NULL,NULL,'11','aa@aa',NULL,'what was the first name of your best childhood friend?','aa','who is your icon?','bb',NULL,NULL),(20,'dummy3',NULL,NULL,NULL,NULL,'11','werwerqwerqwe',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(21,'Al-Amin',NULL,NULL,NULL,NULL,'11','abc@abc',NULL,'what was the first name of your best childhood friend?','aa','who is your icon?','bb',NULL,NULL),(22,'alamin',NULL,NULL,NULL,NULL,'11','ss@dd',NULL,'what was your mother birthplace?','Dhaka','who is your icon?','Neymar',NULL,NULL),(23,'dfgdfgd',NULL,NULL,NULL,NULL,'11','ss@ddd',NULL,'what was your mother birthplace?','erter','what is your favourite sports team?','erter',NULL,NULL),(24,'1ss',NULL,NULL,NULL,NULL,'11','sss@dd',NULL,'what was the first name of your best childhood friend?','sss','what is your favourite sports team?','sssss',NULL,NULL),(25,'asss',NULL,NULL,NULL,NULL,'11','sss@ddd',NULL,'what was the first name of your best childhood friend?','ww','what is your favourite sports team?','ww',NULL,NULL),(26,'gol',NULL,NULL,NULL,NULL,'Riss1*','gol@gmail.com',NULL,'what was the first name of your best childhood friend?','Paris','what is your favourite sports team?','Paris',NULL,NULL),(27,'Gol',NULL,NULL,NULL,NULL,'Riss1*','galil@hotmail.com',NULL,'what was the first name of your best childhood friend?','Paris','what is your favourite sports team?','Paris',NULL,NULL),(28,'Bol',NULL,NULL,NULL,NULL,'Riss1*','bol@gmail.com',NULL,'what was the first name of your best childhood friend?','Paris ','what is your favourite sports team?','Paris',NULL,NULL),(29,'Fol',NULL,NULL,NULL,NULL,'Riss1*','fol@gmail.com',NULL,'what was the first name of your best childhood friend?','Paris','what is your favourite sports team?','Paris',NULL,NULL),(30,'admin',NULL,NULL,NULL,NULL,'kujaza','admin@kujaza.com',NULL,'what was the first name of your best childhood friend?','a','who is your icon?','b',NULL,NULL),(31,'Md. Al- Amin',NULL,NULL,NULL,NULL,'11','s@a',NULL,'what was your mother birthplace?','a','who is your icon?','fdasdfasd',NULL,NULL),(32,'seiler jeanne',NULL,NULL,NULL,NULL,'panpan','jeanne_seiler@hotmail.com',NULL,'what was the first name of your best childhood friend?','célia','who is your icon?','mickael jackson',NULL,NULL),(33,'werqe',NULL,NULL,NULL,NULL,'11','wer@twer',NULL,'what was the first name of your best childhood friend?','11','what is your favourite sports team?','11',NULL,NULL),(36,'Kuja',NULL,NULL,NULL,NULL,'Riss1987*','contact.kujaza@gmail.com',NULL,'what was your mother birthplace?','Paris','what is your favourite sports team?','Paris',1,'62342779'),(37,'',NULL,NULL,NULL,NULL,'Riss1987*','newlogin47@gmail.com',NULL,'what was your mother birthplace?','Paris','what is your favourite sports team?','Paris',1,'9903660'),(38,'aaa',NULL,NULL,NULL,NULL,'11','alaminbbsc@gmail.com',NULL,'what was the first name of your best childhood friend?','11','who is your icon?','11',1,'49684404'),(39,'Drop',NULL,NULL,NULL,NULL,'Riss1987*','dropmayling@hotmail.com',NULL,'what was the first name of your best childhood friend?','Paris','what is your favourite sports team?','Paris',0,'44796842');
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `help`
--

DROP TABLE IF EXISTS `help`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `help` (
  `name` varchar(110) DEFAULT NULL,
  `email` varchar(110) DEFAULT NULL,
  `reason` varchar(1010) DEFAULT NULL,
  `description` varchar(10010) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `help`
--

LOCK TABLES `help` WRITE;
/*!40000 ALTER TABLE `help` DISABLE KEYS */;
INSERT INTO `help` VALUES ('rqwerq','qwer@qwe','qrwerqwer','                                                qwerqwerqwerqwer'),('ey','eyr','eyt','       ey                                         ');
/*!40000 ALTER TABLE `help` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rating`
--

DROP TABLE IF EXISTS `rating`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rating` (
  `id` int(11) NOT NULL,
  `shipper_id` int(11) DEFAULT NULL,
  `message` varchar(1010) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `star` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rating`
--

LOCK TABLES `rating` WRITE;
/*!40000 ALTER TABLE `rating` DISABLE KEYS */;
INSERT INTO `rating` VALUES (0,0,'0',0,0),(1,1,'fvsdfasdfsdfasdfasdfas',22,5),(2,14,'werqwe',22,1),(3,21,'uiouio',22,1),(4,21,'hfghdfg',23,4),(5,21,'zz',24,5),(6,21,'aaaa',25,5),(7,1,'cool man',24,4),(8,27,'Good',29,5);
/*!40000 ALTER TABLE `rating` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shipper`
--

DROP TABLE IF EXISTS `shipper`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shipper` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(110) DEFAULT NULL,
  `img_url` varchar(110) DEFAULT NULL,
  `member_since` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `password` varchar(110) NOT NULL,
  `email` varchar(110) NOT NULL,
  `about_me` varchar(10010) DEFAULT NULL,
  `rating_count` double DEFAULT '0',
  `total_rating_point` double DEFAULT '0',
  `order_count` double DEFAULT '0',
  `question1` varchar(1010) DEFAULT NULL,
  `answer1` varchar(1010) DEFAULT NULL,
  `question2` varchar(1010) DEFAULT NULL,
  `answer2` varchar(1010) DEFAULT NULL,
  `is_verified` int(11) DEFAULT NULL,
  `veification_key` varchar(110) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shipper`
--

LOCK TABLES `shipper` WRITE;
/*!40000 ALTER TABLE `shipper` DISABLE KEYS */;
INSERT INTO `shipper` VALUES (1,'LOL2',NULL,'2017-08-03 02:35:21','2','a@g2','I am Mahabub2',0,0,0,NULL,NULL,NULL,NULL,NULL,NULL),(2,'aaaa',NULL,'2017-08-12 23:37:20','11','a@b',NULL,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL),(3,'Riss1',NULL,'2017-08-20 03:45:41','Riss1','mouhammad87@hotmail.com',NULL,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL),(4,'customersust',NULL,'2017-08-20 04:01:54','12','c@sust','null',0,0,0,NULL,NULL,NULL,NULL,NULL,NULL),(5,'asdfasd',NULL,'2017-08-26 01:19:01','11','shipper@kujaza.com',NULL,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL),(6,'bb',NULL,'2017-09-08 22:10:12','11','bb@bb','null',0,0,0,NULL,NULL,NULL,NULL,NULL,NULL),(7,'Riss',NULL,'2017-09-10 03:32:55','Riss1*','mouhammadmladjao@hotmail.fr','null',0,0,0,NULL,NULL,NULL,NULL,NULL,NULL),(11,'minar rahman',NULL,'2017-11-04 02:24:54','11','minar@rahman',NULL,0,0,0,'what was the first name of your best childhood friend?','aa','who is your icon?','bb',NULL,NULL),(12,'dummy1',NULL,'2017-11-04 02:26:57','11','11',NULL,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL),(14,'dummy2',NULL,'2017-11-04 02:27:16','11','1111',NULL,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL),(16,'dummy3',NULL,'2017-11-04 02:27:22','11','111',NULL,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL),(17,'dummy4',NULL,'2017-11-04 02:27:23','11','11111',NULL,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL),(18,'shahad',NULL,'2017-11-04 02:27:53','11','ss@ss',NULL,0,0,0,'what was the first name of your best childhood friend?','aa','who is your icon?','bb',NULL,NULL),(19,'alamin',NULL,'2017-11-04 02:29:29','11','aa@aa',NULL,0,0,0,'what was the first name of your best childhood friend?','aa','who is your icon?','bb',NULL,NULL),(20,'Al-Amin',NULL,'2017-11-04 02:30:22','11','aa@bb',NULL,0,0,0,'what was the first name of your best childhood friend?','aa','who is your icon?','bb',NULL,NULL),(21,'Al-Amin',NULL,'2017-11-04 02:35:35','11','abc@abc','hiiii I am alamin',0,0,0,'what was the first name of your best childhood friend?','aa','who is your icon?','bb',NULL,NULL),(22,'alamin',NULL,'2017-12-19 03:13:19','11','ss@dd','null',0,0,0,'what was your mother birthplace?','Dhaka','who is your icon?','Neymar',NULL,NULL),(23,'dfgdfgd',NULL,'2017-12-31 05:37:09','11','ss@ddd',NULL,0,0,0,'what was your mother birthplace?','erter','what is your favourite sports team?','erter',NULL,NULL),(24,'1ss',NULL,'2017-12-31 05:45:17','11','sss@dd','null',0,0,0,'what was the first name of your best childhood friend?','sss','what is your favourite sports team?','sssss',NULL,NULL),(25,'asss',NULL,'2017-12-31 05:51:14','11','sss@ddd',NULL,0,0,0,'what was the first name of your best childhood friend?','ww','what is your favourite sports team?','ww',NULL,NULL),(26,'gol',NULL,'2018-01-02 08:27:49','Riss1*','gol@gmail.com',NULL,0,0,0,'what was the first name of your best childhood friend?','Paris','what is your favourite sports team?','Paris',NULL,NULL),(27,'Gol',NULL,'2018-01-03 06:04:09','Riss1*','galil@hotmail.com',NULL,0,0,0,'what was the first name of your best childhood friend?','Paris','what is your favourite sports team?','Paris',NULL,NULL),(28,'Bol',NULL,'2018-01-03 06:09:20','Riss1*','bol@gmail.com',NULL,0,0,0,'what was the first name of your best childhood friend?','Paris ','what is your favourite sports team?','Paris',NULL,NULL),(29,'Fol',NULL,'2018-01-03 06:12:43','Riss1*','fol@gmail.com',NULL,0,0,0,'what was the first name of your best childhood friend?','Paris','what is your favourite sports team?','Paris',NULL,NULL),(30,'admin',NULL,'2018-04-27 16:53:30','kujaza','admin@kujaza.com','null',0,0,0,'what was the first name of your best childhood friend?','a','who is your icon?','b',NULL,NULL),(31,'Md. Al- Amin',NULL,'2018-05-10 12:43:52','11','s@a',NULL,0,0,0,'what was your mother birthplace?','a','who is your icon?','fdasdfasd',NULL,NULL),(32,'seiler jeanne',NULL,'2018-07-09 18:57:34','panpan','jeanne_seiler@hotmail.com','null',0,0,0,'what was the first name of your best childhood friend?','célia','who is your icon?','mickael jackson',NULL,NULL),(33,'werqe',NULL,'2018-07-13 12:51:29','11','wer@twer',NULL,0,0,0,'what was the first name of your best childhood friend?','11','what is your favourite sports team?','11',NULL,NULL),(36,'Kuja',NULL,'2018-07-13 21:56:26','Riss1987*','contact.kujaza@gmail.com',NULL,0,0,0,'what was your mother birthplace?','Paris','what is your favourite sports team?','Paris',1,'62342779'),(37,'',NULL,'2018-07-13 22:06:30','Riss1987*','newlogin47@gmail.com',NULL,0,0,0,'what was your mother birthplace?','Paris','what is your favourite sports team?','Paris',1,'9903660'),(38,'aaa',NULL,'2018-07-14 12:46:47','11','alaminbbsc@gmail.com',NULL,0,0,0,'what was the first name of your best childhood friend?','11','who is your icon?','11',1,'49684404'),(39,'Drop',NULL,'2018-07-14 19:02:56','Riss1987*','dropmayling@hotmail.com',NULL,0,0,0,'what was the first name of your best childhood friend?','Paris','what is your favourite sports team?','Paris',0,'44796842');
/*!40000 ALTER TABLE `shipper` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-23 14:24:25
