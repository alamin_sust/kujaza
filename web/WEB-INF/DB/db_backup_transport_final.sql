-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: transport
-- ------------------------------------------------------
-- Server version	5.7.18-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `container`
--

DROP TABLE IF EXISTS `container`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `container` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(110) NOT NULL,
  `description` varchar(10010) DEFAULT NULL,
  `departure` varchar(110) DEFAULT NULL,
  `arrival` varchar(110) DEFAULT NULL,
  `volume_m3` double DEFAULT NULL,
  `shipper_id` int(11) DEFAULT NULL,
  `price_per_m3` double DEFAULT NULL,
  `shipping_date` varchar(110) DEFAULT NULL,
  `arrival_date` varchar(110) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `container_shipper_id_fk` (`shipper_id`),
  CONSTRAINT `container_shipper_id_fk` FOREIGN KEY (`shipper_id`) REFERENCES `customer` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `container`
--

LOCK TABLES `container` WRITE;
/*!40000 ALTER TABLE `container` DISABLE KEYS */;
INSERT INTO `container` VALUES (1,'345234','dfasdf','qr','qr',12,1,5,'08/19/2017','08/03/2017'),(2,'345234','dfasdf','qr','qr',13,1,5,'08/19/2017','08/03/2017'),(3,'345234','dfasdf','qr','qr',12,1,5,'08/19/2017','08/03/2017'),(4,'345234','dfasdf','qr','qr',342,1,5,'08/19/2017','08/03/2017'),(5,'train','chal niye jabo','dhaka','barisal',NULL,4,26,'08/20/2017','08/20/2017'),(6,'Conteneur1','Je suis pret','Lyon','Paris',NULL,7,630,'09/25/2017','09/27/2017'),(7,'11','111aa','qqq','qas',NULL,6,22,'01/09/2017','09/24/2017'),(8,'twer','twerter','tw','twer',NULL,21,5,'11/19/2017','11/19/2017'),(10,'fasdfa','fasd','sdfa','fs',24,21,6,'12/02/2017','12/14/2017'),(11,'douala','fjfyfjfcjfyk','Paris','bombay',2,26,7,'01/02/2018','01/03/2018'),(12,'jjjj','fjfkut','lyon','paris',9,26,7,'01/02/2018','01/05/2018'),(13,'etet','erter','tewrt','tert',5,24,5,'01/02/2018','01/02/2018'),(14,'Fgvhcv','Fgvbbjj','Paris','Bamako',3,27,8,'01/03/2018','01/04/2018'),(15,'ertwer','ewrter','dfgdf','sdfg',3,22,435,'01/02/2018','01/03/2018');
/*!40000 ALTER TABLE `container` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `conversation`
--

DROP TABLE IF EXISTS `conversation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conversation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender_id` int(11) DEFAULT NULL,
  `sender_type` varchar(110) DEFAULT NULL,
  `receiver_id` int(11) DEFAULT NULL,
  `receiver_type` varchar(110) DEFAULT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `message` varchar(1010) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conversation`
--

LOCK TABLES `conversation` WRITE;
/*!40000 ALTER TABLE `conversation` DISABLE KEYS */;
INSERT INTO `conversation` VALUES (1,1,'shipper',1,'customer','2017-08-19 15:52:10','Hello Customer 1'),(2,1,'customer',1,'shipper','2017-08-19 15:52:10','Hello Shipper 1'),(3,2,'shipper',1,'customer','2017-08-19 15:52:10','Hello MAN!'),(4,1,'shipper',2,'customer','2017-08-19 15:53:16','hello customer 2'),(5,1,'shipper',1,'customer','2017-08-19 19:12:39','hiii'),(6,6,'shipper',1,'customer','2017-09-08 21:01:35','hiii'),(7,6,'shipper',8,'customer','2017-09-08 21:03:18','helllllloooooo'),(8,8,'customer',6,'shipper','2017-09-08 21:04:13','lol'),(9,7,'shipper',1,'customer','2017-09-13 16:32:55','Hello'),(10,21,'shipper',1,'customer','2017-12-01 19:41:49','hiii'),(11,1,'customer',21,'shipper','2017-12-01 19:45:42','yo'),(12,1,'customer',2,'shipper','2017-12-01 19:46:11','yyyyyy');
/*!40000 ALTER TABLE `conversation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(110) DEFAULT NULL,
  `img_url` varchar(110) DEFAULT NULL,
  `address` varchar(110) DEFAULT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `mobile` varchar(30) DEFAULT NULL,
  `password` varchar(110) NOT NULL,
  `email` varchar(110) NOT NULL,
  `about_me` varchar(1010) DEFAULT NULL,
  `question1` varchar(1010) DEFAULT NULL,
  `answer1` varchar(1010) DEFAULT NULL,
  `question2` varchar(1010) DEFAULT NULL,
  `answer2` varchar(1010) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer`
--

LOCK TABLES `customer` WRITE;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
INSERT INTO `customer` VALUES (1,'werqwerqwe',NULL,NULL,NULL,NULL,'11','b@b',NULL,NULL,NULL,NULL,NULL),(2,'frqwer',NULL,NULL,NULL,NULL,'22','c@c',NULL,NULL,NULL,NULL,NULL),(3,'',NULL,NULL,NULL,NULL,'','',NULL,NULL,NULL,NULL,NULL),(4,'riss',NULL,NULL,NULL,NULL,'gio','rissasmusic@hotmail.fr',NULL,NULL,NULL,NULL,NULL),(5,'Al Amin',NULL,NULL,NULL,NULL,'11','alamin@gmail.com',NULL,NULL,NULL,NULL,NULL),(6,'shippersust',NULL,NULL,NULL,NULL,'12','s@sust',NULL,NULL,NULL,NULL,NULL),(7,'Sai',NULL,NULL,NULL,NULL,'Riss','hsaidyi@me.com',NULL,NULL,NULL,NULL,NULL),(15,'minar rahman',NULL,NULL,NULL,NULL,'11','minar@rahman',NULL,'what was the first name of your best childhood friend?','aa','who is your icon?','bb'),(16,'shahad',NULL,NULL,NULL,NULL,'11','ss@ss',NULL,'what was the first name of your best childhood friend?','aa','who is your icon?','bb'),(17,'dummy1',NULL,NULL,NULL,NULL,'11','aaa',NULL,NULL,NULL,NULL,NULL),(18,'dummy2',NULL,NULL,NULL,NULL,'11','aaaa',NULL,NULL,NULL,NULL,NULL),(19,'alamin',NULL,NULL,NULL,NULL,'11','aa@aa',NULL,'what was the first name of your best childhood friend?','aa','who is your icon?','bb'),(20,'dummy3',NULL,NULL,NULL,NULL,'11','werwerqwerqwe',NULL,NULL,NULL,NULL,NULL),(21,'Al-Amin',NULL,NULL,NULL,NULL,'11','abc@abc',NULL,'what was the first name of your best childhood friend?','aa','who is your icon?','bb'),(22,'alamin',NULL,NULL,NULL,NULL,'11','ss@dd',NULL,'what was your mother birthplace?','Dhaka','who is your icon?','Neymar'),(23,'dfgdfgd',NULL,NULL,NULL,NULL,'11','ss@ddd',NULL,'what was your mother birthplace?','erter','what is your favourite sports team?','erter'),(24,'1ss',NULL,NULL,NULL,NULL,'11','sss@dd',NULL,'what was the first name of your best childhood friend?','sss','what is your favourite sports team?','sssss'),(25,'asss',NULL,NULL,NULL,NULL,'11','sss@ddd',NULL,'what was the first name of your best childhood friend?','ww','what is your favourite sports team?','ww'),(26,'gol',NULL,NULL,NULL,NULL,'Riss1*','gol@gmail.com',NULL,'what was the first name of your best childhood friend?','Paris','what is your favourite sports team?','Paris'),(27,'Gol',NULL,NULL,NULL,NULL,'Riss1*','galil@hotmail.com',NULL,'what was the first name of your best childhood friend?','Paris','what is your favourite sports team?','Paris'),(28,'Bol',NULL,NULL,NULL,NULL,'Riss1*','bol@gmail.com',NULL,'what was the first name of your best childhood friend?','Paris ','what is your favourite sports team?','Paris'),(29,'Fol',NULL,NULL,NULL,NULL,'Riss1*','fol@gmail.com',NULL,'what was the first name of your best childhood friend?','Paris','what is your favourite sports team?','Paris');
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `help`
--

DROP TABLE IF EXISTS `help`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `help` (
  `name` varchar(110) DEFAULT NULL,
  `email` varchar(110) DEFAULT NULL,
  `reason` varchar(1010) DEFAULT NULL,
  `description` varchar(10010) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `help`
--

LOCK TABLES `help` WRITE;
/*!40000 ALTER TABLE `help` DISABLE KEYS */;
INSERT INTO `help` VALUES ('rqwerq','qwer@qwe','qrwerqwer','                                                qwerqwerqwerqwer'),('ey','eyr','eyt','       ey                                         ');
/*!40000 ALTER TABLE `help` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_`
--

DROP TABLE IF EXISTS `order_`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(110) DEFAULT NULL,
  `description` varchar(110) DEFAULT NULL,
  `shipper_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `container_id` int(11) DEFAULT NULL,
  `is_confirmed` int(11) DEFAULT NULL,
  `review` varchar(1010) DEFAULT NULL,
  `star` int(11) DEFAULT NULL,
  `delivered` int(11) DEFAULT '0',
  `price` double DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `order_container_id_fk` (`container_id`),
  KEY `order_shipper_id_fk` (`shipper_id`),
  KEY `order_seller_id_fk` (`customer_id`),
  CONSTRAINT `order_container_id_fk` FOREIGN KEY (`container_id`) REFERENCES `container` (`id`),
  CONSTRAINT `order_seller_id_fk` FOREIGN KEY (`customer_id`) REFERENCES `shipper` (`id`),
  CONSTRAINT `order_shipper_id_fk` FOREIGN KEY (`shipper_id`) REFERENCES `customer` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_`
--

LOCK TABLES `order_` WRITE;
/*!40000 ALTER TABLE `order_` DISABLE KEYS */;
INSERT INTO `order_` VALUES (1,'',NULL,1,1,1,1,'good shipper',4,1,0),(2,'',NULL,1,1,1,0,'Poor Shipper',3,0,0);
/*!40000 ALTER TABLE `order_` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rating`
--

DROP TABLE IF EXISTS `rating`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rating` (
  `id` int(11) NOT NULL,
  `shipper_id` int(11) DEFAULT NULL,
  `message` varchar(1010) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `star` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rating`
--

LOCK TABLES `rating` WRITE;
/*!40000 ALTER TABLE `rating` DISABLE KEYS */;
INSERT INTO `rating` VALUES (0,0,'0',0,0),(1,1,'fvsdfasdfsdfasdfasdfas',22,5),(2,14,'werqwe',22,1),(3,21,'uiouio',22,1),(4,21,'hfghdfg',23,4),(5,21,'zz',24,5),(6,21,'aaaa',25,5),(7,1,'cool man',24,4),(8,27,'Good',29,5);
/*!40000 ALTER TABLE `rating` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shipper`
--

DROP TABLE IF EXISTS `shipper`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shipper` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(110) DEFAULT NULL,
  `img_url` varchar(110) DEFAULT NULL,
  `member_since` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `password` varchar(110) NOT NULL,
  `email` varchar(110) NOT NULL,
  `about_me` varchar(10010) DEFAULT NULL,
  `rating_count` double DEFAULT '0',
  `total_rating_point` double DEFAULT '0',
  `order_count` double DEFAULT '0',
  `question1` varchar(1010) DEFAULT NULL,
  `answer1` varchar(1010) DEFAULT NULL,
  `question2` varchar(1010) DEFAULT NULL,
  `answer2` varchar(1010) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shipper`
--

LOCK TABLES `shipper` WRITE;
/*!40000 ALTER TABLE `shipper` DISABLE KEYS */;
INSERT INTO `shipper` VALUES (1,'LOL2',NULL,'2017-08-02 19:35:21','2','a@g2','I am Mahabub2',0,0,0,NULL,NULL,NULL,NULL),(2,'aaaa',NULL,'2017-08-12 16:37:20','11','a@b',NULL,0,0,0,NULL,NULL,NULL,NULL),(3,'Riss1',NULL,'2017-08-19 20:45:41','Riss1','mouhammad87@hotmail.com',NULL,0,0,0,NULL,NULL,NULL,NULL),(4,'customersust',NULL,'2017-08-19 21:01:54','12','c@sust','null',0,0,0,NULL,NULL,NULL,NULL),(5,'asdfasd',NULL,'2017-08-25 18:19:01','11','shipper@kujaza.com',NULL,0,0,0,NULL,NULL,NULL,NULL),(6,'bb',NULL,'2017-09-08 15:10:12','11','bb@bb','null',0,0,0,NULL,NULL,NULL,NULL),(7,'Riss',NULL,'2017-09-09 20:32:55','Riss1*','mouhammadmladjao@hotmail.fr','null',0,0,0,NULL,NULL,NULL,NULL),(11,'minar rahman',NULL,'2017-11-03 19:24:54','11','minar@rahman',NULL,0,0,0,'what was the first name of your best childhood friend?','aa','who is your icon?','bb'),(12,'dummy1',NULL,'2017-11-03 19:26:57','11','11',NULL,0,0,0,NULL,NULL,NULL,NULL),(14,'dummy2',NULL,'2017-11-03 19:27:16','11','1111',NULL,0,0,0,NULL,NULL,NULL,NULL),(16,'dummy3',NULL,'2017-11-03 19:27:22','11','111',NULL,0,0,0,NULL,NULL,NULL,NULL),(17,'dummy4',NULL,'2017-11-03 19:27:23','11','11111',NULL,0,0,0,NULL,NULL,NULL,NULL),(18,'shahad',NULL,'2017-11-03 19:27:53','11','ss@ss',NULL,0,0,0,'what was the first name of your best childhood friend?','aa','who is your icon?','bb'),(19,'alamin',NULL,'2017-11-03 19:29:29','11','aa@aa',NULL,0,0,0,'what was the first name of your best childhood friend?','aa','who is your icon?','bb'),(20,'Al-Amin',NULL,'2017-11-03 19:30:22','11','aa@bb',NULL,0,0,0,'what was the first name of your best childhood friend?','aa','who is your icon?','bb'),(21,'Al-Amin',NULL,'2017-11-03 19:35:35','11','abc@abc','hiiii I am alamin',0,0,0,'what was the first name of your best childhood friend?','aa','who is your icon?','bb'),(22,'alamin',NULL,'2017-12-18 19:13:19','11','ss@dd','null',0,0,0,'what was your mother birthplace?','Dhaka','who is your icon?','Neymar'),(23,'dfgdfgd',NULL,'2017-12-30 21:37:09','11','ss@ddd',NULL,0,0,0,'what was your mother birthplace?','erter','what is your favourite sports team?','erter'),(24,'1ss',NULL,'2017-12-30 21:45:17','11','sss@dd','null',0,0,0,'what was the first name of your best childhood friend?','sss','what is your favourite sports team?','sssss'),(25,'asss',NULL,'2017-12-30 21:51:14','11','sss@ddd',NULL,0,0,0,'what was the first name of your best childhood friend?','ww','what is your favourite sports team?','ww'),(26,'gol',NULL,'2018-01-02 00:27:49','Riss1*','gol@gmail.com',NULL,0,0,0,'what was the first name of your best childhood friend?','Paris','what is your favourite sports team?','Paris'),(27,'Gol',NULL,'2018-01-02 22:04:09','Riss1*','galil@hotmail.com',NULL,0,0,0,'what was the first name of your best childhood friend?','Paris','what is your favourite sports team?','Paris'),(28,'Bol',NULL,'2018-01-02 22:09:20','Riss1*','bol@gmail.com',NULL,0,0,0,'what was the first name of your best childhood friend?','Paris ','what is your favourite sports team?','Paris'),(29,'Fol',NULL,'2018-01-02 22:12:43','Riss1*','fol@gmail.com',NULL,0,0,0,'what was the first name of your best childhood friend?','Paris','what is your favourite sports team?','Paris');
/*!40000 ALTER TABLE `shipper` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-01-03  4:51:24
