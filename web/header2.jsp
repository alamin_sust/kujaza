<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta charset="utf-8" />
<link href="assets/css/bootstrap.min.css" rel="stylesheet" />
<link href="assets/css/main.css" rel="stylesheet" />
<link href="assets/css/animate.css" rel="stylesheet" />
<link href="assets/css/aos.css" rel="stylesheet" />
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
<link href="assets/css/tabs.css" rel="stylesheet" />
<link href="assets/css/tabstyles.css" rel="stylesheet" />
<link href="assets/css/DatePicker.css" rel="stylesheet" />

<%

            Statement stH = db.connection.createStatement();
            String qH = "select * from "+session.getAttribute("service")+" where email='"+session.getAttribute("email")+"'";
            ResultSet rsH;
            rsH=stH.executeQuery(qH);
            
            rsH.next();
            
             String idH=rsH.getString("id");


%>


<nav id="MenuNotLogged" class="navbar bg-col navbar-inverse" style="border: green">
    <div class="container-fluid">

        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span> 
            </button>
            <a class="" href="index.jsp">
                <img class="img-responsive animated zoomIn" style="margin-top: 5px; width: 90px; height: 40px;" src="assets/img/logo_white.png" />
            </a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav LoggedNav navbar-nav">
                <%if(session.getAttribute("lang")!=null){%>
                                <li><a href="index.jsp?lang=1"><span ></span> <img src="assets/img/fl-fr.png"/></a></li>
                                <%}else{%>
                                <li><a href="index.jsp?lang=0"><span ></span> <img src="assets/img/fl-uk.png"/></a></li>
                                <%}%>
                <li><a href="addContainer.jsp"><span class="fa fa-plus"></span> <%=tr[lang][71]%></a></li>
                <li><a href="conversation.jsp"><span class="fa fa-envelope"></span> <%=tr[lang][72]%></a></li>
                <%--<li><a href="#"><span class="fa fa-bar-chart"></span> <%=tr[lang][73]%></a></li> --%>
                <li><a href="shipperProfile.jsp?sId=<%=rsH.getString("id")%>"><span class="fa fa-bar-chart"></span> <%=tr[lang][191]%></a></li>
                <li><a href="containerList.jsp?sId=<%=rsH.getString("id")%>"><span class="fa fa-bar-chart"></span> <%=tr[lang][192]%></a></li>
                <li><a href="dashBoard.jsp?sId=<%=rsH.getString("id")%>"><span class="fa fa-dashboard"></span> <%=tr[lang][188]%></a></li>
                
            </ul>
            <ul id="NotLoggedRight" class="nav hidden navbar-nav navbar-right">
<!--                <li><a href="#" data-toggle="modal" data-target="#SearchForm"><span class="fa fa-search"></span> Search</a></li>-->
                    <li><a href="index.jsp"><span class="fa fa-search"></span> <%=tr[lang][75]%></a></li>
                <li><a href="#"><span class="fa fa-sign-in"></span> <%=tr[lang][76]%> / <span class="fa fa-user"></span> <%=tr[lang][77]%></a></li>
            </ul>
            <ul id="LoggedRight" class="nav navbar-nav navbar-right">
<!--                <li><a href="#" data-toggle="modal" data-target="#SearchForm"><span class="fa fa-search"></span> Search</a></li>-->
                    <li><a href="index.jsp"><span class="fa fa-search"></span> <%=tr[lang][78]%></a></li>
                    <li><a class="no-padding dropdown-toggle" data-toggle="dropdown" href="#"><img class="img-responsive img-user img-circle" style="white:45px; height: 45px;" src="assets/img/users/<%=session.getAttribute("service")%><%=idH%>.jpg" /><span class="caret"></span></a>
                    <ul class="dropdown-menu animated flipInX">
                        <li class="menuUN"><%=session.getAttribute("name")%></li>
                        <li><a href="#"><%=tr[lang][79]%></a></li>
                        <li><a href="settings.jsp"><%=tr[lang][80]%></a></li>
                        <li><a href="help.jsp"><%=tr[lang][81]%></a></li>
                        <li><a href="#"><%=tr[lang][82]%></a></li>
                        <li><a href="login.jsp" class="aksBtn"><%=tr[lang][83]%> <span class="fa fa-sign-out"></span></a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>