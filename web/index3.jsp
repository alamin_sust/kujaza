
?<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <%@ include file="lang.jsp" %>
    <title>Kujaza &bull; <%=tr[lang][102]%></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta charset="utf-8" />
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/main.css" rel="stylesheet" />
    <link href="assets/css/animate.css" rel="stylesheet" />
    <link href="assets/css/aos.css" rel="stylesheet" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
    <link href="assets/css/DatePicker.css" rel="stylesheet" />
</head>
<body>
    <!-- HEADER -->
    <%@ include file="header.jsp" %>
    <!-- END HEADER -->

    <div id="myCarousel" class="carousel slide" data-pause="false" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>
        <div class="searchForm animated fadeInUp">
            <h2 style="font-size: 40px;"><b><%=tr[lang][103]%></b></h2>
            <!--<i class="line"></i>-->  
            <form action="searchResult.jsp" method="get">
                <div class="row">
                    <div class="col col-sm-1" style="padding-bottom:10px;"></div>
                     <div class="col col-sm-3" style="padding-bottom:10px;">   <input type="text" class="form-control" name="departure" placeholder="Departure From" /></div>
                <div class="col col-sm-3" style="padding-bottom:10px;"><input type="text" class="form-control"  name="arrival" placeholder="Arrival to" /></div>
                <div class="col col-sm-3" style="padding-bottom:10px;"><input type="text" class="form-control"  name="shippingDate" id="txtShippingDate" placeholder="Shipping Date" /></div>
                </div>
            <center><button type="submit" class="aksBtn this2"><%=tr[lang][104]%> <span class="fa fa-search"></span></button></center>
            </form>
        </div>
      <!-- Wrapper for slides -->
      <div class="carousel-inner">
        <div class="item active i1">
        </div>

        <div class="item i2">
        </div>

        <div class="item i3">
        </div>
      </div>
    </div>


    <div class="section-even container-fluid">
        <h2 style="font-size: 30px;"><b><%=tr[lang][105]%></b></h2>
        <i class="line"></i>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <img class="img-responsive img" src="assets/img/control.png" />
                <p class="title" style="font-size: 20px;"><b><%=tr[lang][106]%></b></p>
                <p class="txt"><%=tr[lang][107]%></p>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <img class="img-responsive img" src="assets/img/money.png" />
                <p class="title" style="font-size: 20px;"><b><%=tr[lang][108]%></b></p>
                <p class="txt"><%=tr[lang][109]%></p>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <img class="img-responsive img" src="assets/img/secure.png" />
                <p class="title" style="font-size: 20px;"><b><%=tr[lang][110]%></b></p>
                <p class="txt"><%=tr[lang][111]%></p>
            </div>
        </div>
    </div>

    <div class="section-odd container-fluid">
        <h2 style="font-size: 30px;"><b><%=tr[lang][112]%></b></h2>
        <i class="line"></i>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <img class="img-responsive img" src="assets/img/signin.png" />
                <p class="title" style="font-size: 20px;"><b><%=tr[lang][113]%></b></p>
                <p class="txt"><%=tr[lang][114]%></p>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <img class="img-responsive img" src="assets/img/search.png" />
                <p class="title" style="font-size: 20px;"><b><%=tr[lang][115]%></b></p>
                <p class="txt"><%=tr[lang][116]%></p>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <img class="img-responsive img" src="assets/img/deal.png" />
                <p class="title" style="font-size: 20px;"><b><%=tr[lang][117]%></b></p>
                <p class="txt"><%=tr[lang][118]%></p>
            </div>
        </div>
    </div>

    <div class="startNow">
        <center><h2 style="font-size: 30px;"><b><%=tr[lang][119]%></b> </h2><span><button class="aksBtn" onclick="location.href = 'login.jsp'"><b style="font-size: 30px;"><%=tr[lang][120]%></b><span class="fa fa-check"></span></button></span></center>
    </div>
    <!-- Scripts -->
    <script src="assets/js/jquery-3.1.1.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/classie.js"></script>
    <script src="assets/js/KeepOpen.js"></script>
    <script src="assets/js/typed.min.js"></script>
    <script src="assets/js/aos.js"></script>
    <script>AOS.init();</script>
    <script src="assets/js/main.js"></script>
    <script src="assets/js/DatePicker.js"></script>
</body>
</html>
