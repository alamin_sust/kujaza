<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="com.kujaza.connection.Database"%>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">
        <%@ include file="lang.jsp" %>
        <title><%=tr[lang][0]%> &bull; KUJAZA</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta charset="utf-8" />
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
        <link href="assets/css/main.css" rel="stylesheet" />
        <link href="assets/css/animate.css" rel="stylesheet" />
        <link href="assets/css/aos.css" rel="stylesheet" />
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
        <link href="assets/css/tabs.css" rel="stylesheet" />
        <link href="assets/css/tabstyles.css" rel="stylesheet" />
        <link href="assets/css/DatePicker.css" rel="stylesheet" />
    </head>
    <body class="bodybg">
        <%
            if (session.getAttribute("email") == null||!session.getAttribute("email").toString().equals("admin@kujaza.com")) {
                response.sendRedirect("login.jsp");
            }
            
            
            Database db = new Database();
            db.connect();

            //try {

                Statement st0 = db.connection.createStatement();
                String q0 = "select * from customer";
                ResultSet rs0 = st0.executeQuery(q0);
                
                

                

        %>

        <div class="row">
            <h2 style="text-align: center">CUSTOMER</h2>
            <div class="col col-sm-10 col-sm-offset-1">
            <table class="table table-striped table-responsive table-bordered">
                <tr>
                    <th>ID</th>
                    <th>NAME</th>
                    <th>EMAIL</th>
                </tr>
                <%while(rs0.next()){
                    String id = rs0.getString("id");
                
                String name = rs0.getString("name");
                
                String email = rs0.getString("email");
                %>
                <tr>
                    <td><%=id%></td>
                    <td><%=name%></td>
                    <td><%=email%></td>
                </tr>
                <%}%>
            </table>
            </div>
        </div>

            <%
            

                Statement st1 = db.connection.createStatement();
                String q1 = "select * from shipper";
                ResultSet rs1 = st1.executeQuery(q1);
                
                

                

        %>
        <br>
        <div class="row">
            <h2 style="text-align: center">SHIPPER</h2>
            <div class="col col-sm-10 col-sm-offset-1">
            <table class="table table-striped table-responsive table-bordered">
                <tr>
                    <th>ID</th>
                    <th>NAME</th>
                    <th>EMAIL</th>
                </tr>
                <%while(rs1.next()){
                    String id = rs1.getString("id");
                
                String name = rs1.getString("name");
                
                String email = rs1.getString("email");
                %>
                <tr>
                    <td><%=id%></td>
                    <td><%=name%></td>
                    <td><%=email%></td>
                </tr>
                <%}%>
            </table>
            </div>
        </div>
            
            <!-- Scripts -->
            <script src="assets/js/jquery-3.1.1.min.js"></script>
            <script src="assets/js/bootstrap.min.js"></script>
            <script src="assets/js/classie.js"></script>
            <script src="assets/js/KeepOpen.js"></script>
            <script src="assets/js/typed.min.js"></script>
            <script src="assets/js/aos.js"></script>
            <script>AOS.init();</script>
            <script src="assets/js/main.js"></script>
            <script src="assets/js/DatePicker.js"></script>
            <script src="assets/js/cbpFWTabs.js"></script>
            <script>
                (function () {

                    [].slice.call(document.querySelectorAll('.tabs')).forEach(function (el) {
                        new CBPFWTabs(el);
                    });

                })();

                $('.acc-type').click(function () {
                    $('.acc-type').removeClass('this-type');
                    $('.acc-type-chkd').removeAttr('checked');
                    $(this).addClass('this-type');
                    $(this).next('.acc-type-chkd').attr('checked', 'checked');
                });

                $(document).ready(function () {
                    $('[data-toggle="tooltip"]').tooltip();
                });
            </script>
        </div>
        <% // } catch (Exception e) {
            //    System.out.println(e);
           // } finally {
           //     db.close();
          //  }
        %>
    </body>
</html>
