<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="com.kujaza.connection.Database"%>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">

        <%@ include file="lang.jsp" %> 

        <title><%=tr[lang][53]%> &bull; Kujaza &bull; <%=tr[lang][54]%></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta charset="utf-8" />
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
        <link href="assets/css/main.css" rel="stylesheet" />
        <link href="assets/css/animate.css" rel="stylesheet" />
        <link href="assets/css/aos.css" rel="stylesheet" />
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
        <link href="assets/css/DatePicker.css" rel="stylesheet" />
    </head>
    <body class="bodybg this">
        <%
            Database db = new Database();
            db.connect();

            try {

                if (session.getAttribute("email") == null) {
                    response.sendRedirect("index.jsp");
                }

                String shipperId = request.getParameter("sId");

                Statement st0 = db.connection.createStatement();
                Statement st1 = db.connection.createStatement();
                Statement st2 = db.connection.createStatement();
                String q0 = "";
                String q1 = "";
                String q2 = "";
                ResultSet rs0;
                ResultSet rs1;
                ResultSet rs2;

                q1 = "select * from shipper where id=" + shipperId;

                rs1 = st1.executeQuery(q1);

                String sId = "";
                String sName = "";
                String sImgUrl = "";
                String sRatingCount = "";
                String sTotalRatingPoint = "";
                String sOrderCount = "";
                String sMemberSince = "";
                String sAboutMe = "";
                double sRating = 0.0;

                while (rs1.next()) {
                    sId = rs1.getString("id");
                    sName = rs1.getString("name");
                    sImgUrl = rs1.getString("img_url");
                    sRatingCount = rs1.getString("rating_count");
                    sTotalRatingPoint = rs1.getString("total_rating_point");
                    sOrderCount = rs1.getString("order_count");
                    sMemberSince = rs1.getString("member_since");
                    sAboutMe = rs1.getString("about_me");
                    if (Double.parseDouble(sRatingCount) != 0.0) {
                        sRating = Double.parseDouble(sTotalRatingPoint) / Double.parseDouble(sRatingCount);
                    }

                }


        %>

        <div>

            <!-- HEADER -->
            <%@ include file="header2.jsp" %>
            <!-- END HEADER -->

            <%            q2 = "select * from order_ where shipper_id=" + shipperId;
                rs2 = st2.executeQuery(q2);
                int totalOrders = 0;
                int activeOrders = 0;
                int pendingOrders = 0;
                double priceEarned = 0;
                double pricePending = 0;
                double priceTotal = 0;

                while (rs2.next()) {
                    if (rs2.getString("is_confirmed").equals("0")) {
                        pendingOrders++;
                    } else if (!rs2.getString("delivered").equals("1")) {
                        activeOrders++;
                        pricePending += rs2.getDouble("price");
                    }
                    totalOrders++;
                    priceTotal += rs2.getDouble("price");
                }
                priceEarned = priceTotal - pricePending;


            %>

            <div class="container-fluid martop0">
                <%--        <div class="row Summary">
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                               <span class="block numberDashboard" id="sumTotalOrders"><%=totalOrders%></span>
                               Total Orders
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                               <span class="block numberDashboard" id="sumActiveOrders"><%=activeOrders%></span>
                                Active Orders
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <span class="block numberDashboard" id="sumPendingOrders"><%=pendingOrders%></span>
                                Pending Orders
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <span class="block numberDashboard" id="sumTotalCurrent"><%=priceEarned%>$ - <%=pricePending%>$</span>
                                Total Earnings - Current Orders
                            </div>
                <!--            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                                <span class="block numberDashboard" id="sumTotalMonth"><small>$</small>2800</span>
                                Total Earnings This Month
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                                <span class="block numberDashboard" id="sumEarnings"><small>$</small>14530</span>
                                Total Earnings All Time
                            </div>-->
                        </div>
                --%>

                <div class="row martop2">
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <div class="sellerInfo animated slideInLeft">
                            <p class="sellerInfoTitle" style="font-size: 25px;"><b><%=tr[lang][55]%></b></p>
                            <hr />
                            <i class="martop7"></i>
                            <img class="img-responsive img-circle center-block" src="assets/img/users/<%=request.getParameter("sId")%>.jpg" />
                            <div class="row">
                                <label id="lblSellerName" class="center-block" style="font-size: 25px;"><b><%=sName%></b></label>
                                <p class="ReviewNumber"><span class="fa fa-star"></span><span><%=sRating%></span> (<%=sRatingCount%>)</p>
                            </div>
                            <div class="row">
                                <label class="aksLbl"><%=tr[lang][56]%> <span id="lblOrderCountGig"> <%=sOrderCount%></span></label>
                            </div>
                            <div class="row">
                                <label class="aksLbl"><%=tr[lang][57]%> <span id="lblMemberDate"> <%=sMemberSince%></span></label>
                            </div>
                            <div class="row form-inline">
                                <%--<button class="aksBtn" onclick="location.href='shipperProfile.jsp?sId=<%=sId%>'"><%=tr[lang][58]%> <span class="fa fa-envelope"></span></button>--%>
                                <form type="get" action="settings.jsp">
                                    <button class="aksBtn" type="submit"><%=tr[lang][189]%> <span class="fa fa-envelope"></span></button>
                                </form>

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                        <div class="todo-list animated slideInRight">
                            <p class="sellerInfoTitle" style="font-size: 25px;"><b><%=tr[lang][190]%></b></p>
                            <hr />
                            <table class="todo-tbl table-responsive">

                                <%
                                    Statement st3 = db.connection.createStatement();
                                    String q3 = "";
                                    ResultSet rs3;
                                    q3 = "select * from order_ where shipper_id=" + shipperId;
                                    rs3 = st3.executeQuery(q3);

                                    while (rs3.next()) {
                                        Statement st4 = db.connection.createStatement();
                                        String q4 = "select * from customer where id=" + rs3.getString("customer_id");
                                        ResultSet rs4 = st4.executeQuery(q4);
                                        if(!rs4.next()){
                                            continue;
                                        }

                                        Statement st5 = db.connection.createStatement();
                                        String q5 = "select * from container where id=" + rs3.getString("container_id");
                                        ResultSet rs5 = st5.executeQuery(q5);
                                        rs5.next();

                                %>


                                <tr>
                                    <td><img class="img-responsive img-circle center-block" src="http://via.placeholder.com/70x70" /></td>
                                    <td><%=tr[lang][60]%> <span><%=rs4.getString("name")%></span>, <%=tr[lang][62]%> <span><%=rs5.getString("shipping_date")%></span> <%=tr[lang][63]%> <span><%=rs5.getString("arrival_date")%></span> <%=tr[lang][64]%> <span><%=rs5.getString("departure")%></span> <%=tr[lang][65]%> <span><%=rs5.getString("arrival")%></span>.</td>
                                </tr>
                                <%}%>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Scripts -->
            <script src="assets/js/jquery-3.1.1.min.js"></script>
            <script src="assets/js/bootstrap.min.js"></script>
            <script src="assets/js/classie.js"></script>
            <script src="assets/js/KeepOpen.js"></script>
            <script src="assets/js/typed.min.js"></script>
            <script src="assets/js/aos.js"></script>
            <script>AOS.init();</script>
            <script src="assets/js/main.js"></script>
            <script src="assets/js/DatePicker.js"></script>
        </div>

        <%
            } catch (Exception e) {
                System.out.println(e);
            } finally {
                db.close();
            }
        %>
    </body>
</html>
