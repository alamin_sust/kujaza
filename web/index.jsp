<!DOCTYPE html>
<%@page import="com.kujaza.connection.Database"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <%@ include file="lang.jsp" %>
    <title>Kujaza &bull; <%=tr[lang][102]%></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta charset="utf-8" />
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/main.css" rel="stylesheet" />
    <link href="assets/css/animate.css" rel="stylesheet" />
    <link href="assets/css/aos.css" rel="stylesheet" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
    <link href="assets/css/DatePicker.css" rel="stylesheet" />
</head>
<body>
    <!-- HEADER -->
    <%@ include file="header.jsp" %>
    <!-- END HEADER -->
<%
            

            try {

                Statement st0 = db.connection.createStatement();
                Statement st1 = db.connection.createStatement();
                Statement st2 = db.connection.createStatement();
                String q0 = "";
                String q1 = "";
                String q2 = "";
                ResultSet rs0;
                ResultSet rs1;
                ResultSet rs2;
                
                q0 = "select * from cities order by city ";
                rs0=st0.executeQuery(q0);


        %>
    <div id="myCarousel" class="carousel slide" data-pause="false" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>
        <div class="searchForm animated fadeInUp">
            <h2 style="font-size: 40px;"><b><%=tr[lang][103]%></b></h2>
            <!--<i class="line"></i>-->  
            <form action="searchResult.jsp" method="get">
                <div class="row">
                    <div class="col col-sm-1" style="padding-bottom:10px;"></div>
                
                    <div class="col col-sm-3" style="padding-bottom:10px;">
                        <select name="departure" class="form-control">
                            <option value=""><%=tr[lang][201]%></option>
                            
                            <%
                            while(rs0.next()) {
                            %>
                            <option value="<%=rs0.getString("city")%>, <%=rs0.getString("country")%>"><%=rs0.getString("city")%>, <%=rs0.getString("country")%></option>
                            <%}%>
                        </select>
                    </div>
                            
                    <div class="col col-sm-3" style="padding-bottom:10px;">
                        <select name="arrival" class="form-control">
                            <option value=""><%=tr[lang][202]%></option>
                            <%
                                rs0.beforeFirst();
                            while(rs0.next()) {
                            %>
                            <option value="<%=rs0.getString("city")%>, <%=rs0.getString("country")%>"><%=rs0.getString("city")%>, <%=rs0.getString("country")%></option>
                            <%}%>
                        </select>
                    </div>
                    
                <div class="col col-sm-3" style="padding-bottom:10px;"><input type="text" class="form-control"  name="shippingDate" id="txtShippingDate" placeholder="<%=tr[lang][203]%>" /></div>
                </div>
            <center><button type="submit" class="aksBtn this2"><%=tr[lang][104]%> <span class="fa fa-search"></span></button></center>
            </form>
        </div>
      <!-- Wrapper for slides -->
      <div class="carousel-inner">
        <div class="item active i1">
        </div>

        <div class="item i2">
        </div>

        <div class="item i3">
        </div>
      </div>
    </div>


    <div class="section-even container-fluid">
        <h2 style="font-size: 30px;"><b><%=tr[lang][245]%></b></h2>
        <i class="line"></i>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <img class="img-responsive img" src="assets/img/control.png" />
                <p class="title" style="font-size: 20px;"><b><%=tr[lang][246]%></b></p>
                <p class="txt"><b><%=tr[lang][247]%></b> <%=tr[lang][248]%></p>
                <p class="txt"><b><%=tr[lang][249]%></b> <%=tr[lang][250]%></p>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <img class="img-responsive img" src="assets/img/money.png" />
                <p class="title" style="font-size: 20px;"><b><%=tr[lang][251]%></b></p>
                <p class="txt"><b><%=tr[lang][252]%></b> <%=tr[lang][253]%></p>
                <p class="txt"><b><%=tr[lang][254]%></b> <%=tr[lang][255]%></p>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <img class="img-responsive img" src="assets/img/secure.png" />
                <p class="title" style="font-size: 20px;"><b><%=tr[lang][256]%></b></p>
                <p class="txt"><%=tr[lang][257]%></p>
            </div>
        </div>
    </div>

    <div class="section-odd container-fluid">
        <h2 style="font-size: 30px;"><b><%=tr[lang][258]%></b></h2>
        <i class="line"></i>
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <img class="img-responsive img" src="assets/img/signin.png" />
                <p class="title" style="font-size: 20px;"><b><%=tr[lang][259]%></b></p>
                <p class="title" style="font-size: 20px;"><b><%=tr[lang][260]%></b></p>
                <p class="txt"><%=tr[lang][261]%></p>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <img class="img-responsive img" src="assets/img/search.png" />
                <p class="title" style="font-size: 20px;"><b><%=tr[lang][262]%></b></p>
                <p class="txt"><b><%=tr[lang][263]%></b></p>
                <p class="txt"><%=tr[lang][264]%></p>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <img class="img-responsive img" src="assets/img/deal.png" />
                <p class="title" style="font-size: 20px;"><b><%=tr[lang][265]%></b></p>
                <p class="txt"><b><%=tr[lang][266]%></b></p>
                <p class="txt"><%=tr[lang][267]%></p>
            </div>
            
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <img class="img-responsive img" src="assets/img/deal.png" />
                <p class="title" style="font-size: 20px;"><b><%=tr[lang][268]%></b></p>
                <p class="txt"><b><%=tr[lang][269]%></b></p>
                <p class="txt"><%=tr[lang][270]%></p>
            </div>
        </div>
    </div>
<%
            } catch (Exception e) {
                System.out.println(e);
            } finally {
                db.close();
            }
        %>

    <div class="startNow">
        <center><h2 style="font-size: 30px;"><b><%=tr[lang][119]%></b> </h2><span><button class="aksBtn" onclick="location.href = 'login.jsp'"><b style="font-size: 30px;"><%=tr[lang][120]%></b><span class="fa fa-check"></span></button></span></center>
    </div>
    <!-- Scripts -->
    <script src="assets/js/jquery-3.1.1.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/classie.js"></script>
    <script src="assets/js/KeepOpen.js"></script>
    <script src="assets/js/typed.min.js"></script>
    <script src="assets/js/aos.js"></script>
    <script>AOS.init();</script>
    <script src="assets/js/main.js"></script>
    <script src="assets/js/DatePicker.js"></script>
</body>
</html>
