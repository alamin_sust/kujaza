﻿$(document).ready(function () {
    var windowHeight = $(window).height();
    $('.firstSection').css('height', windowHeight);
    $('.i1').css('height', windowHeight);
    $('.i2').css('height', windowHeight);
    $('.i3').css('height', windowHeight);
    $('.formBox').css('min-height', windowHeight);
});

$(function () {
    $(".element").typed({
        strings: ["making money.", "being successful.", "with Kujaza.^2500"],
        typeSpeed: 40,
        loop: true,
        loopCount: null
    });
});

$(document).ready(function () {
    var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
                $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            //$target.find('input:eq(0)').focus();
        }
    });

    allNextBtn.click(function () {
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='url']"),
            isValid = true;

        $(".form-group").removeClass("has-error");
        for (var i = 0; i < curInputs.length; i++) {
            if (!curInputs[i].validity.valid) {
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }

        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });

    $('div.setup-panel div a.btn-primary').trigger('click');
});

$(window).scroll(function () {
    var scrollTop = $(window).scrollTop();
    if (scrollTop>100) {
        $('#MenuMain').addClass('bg-col');
    }
    else
    {
        $('#MenuMain').removeClass('bg-col');
    }
});

$(function () {

    var defaultselectbox = $('#cusSelectbox');
    var numOfOptions = $('#cusSelectbox').children('option').length;

    // hide select tag
    defaultselectbox.addClass('s-hidden');

    // wrapping default selectbox into custom select block
    defaultselectbox.wrap('<div class="cusSelBlock"></div>');

    // creating custom select div
    defaultselectbox.after('<div class="selectLabel"></div>');

    // getting default select box selected value
    $('.selectLabel').text(defaultselectbox.children('option').eq(0).text());

    // appending options to custom un-ordered list tag
    var cusList = $('<ul/>', { 'class': 'options' }).insertAfter($('.selectLabel'));

    // generating custom list items
    for (var i = 0; i < numOfOptions; i++) {
        $('<li/>', {
            text: defaultselectbox.children('option').eq(i).text(),
            rel: defaultselectbox.children('option').eq(i).val()
        }).appendTo(cusList);
    }

    // open-list and close-list items functions
    function openList() {
        for (var i = 0; i < numOfOptions; i++) {
            $('.options').children('li').eq(i).attr('tabindex', i).css(
				'transform', 'translateY(' + (i * 100 + 100) + '%)').css(
				'transition-delay', i * 30 + 'ms');
        }
    }

    function closeList() {
        for (var i = 0; i < numOfOptions; i++) {
            $('.options').children('li').eq(i).css(
				'transform', 'translateY(' + i * 0 + 'px)').css('transition-delay', i * 0 + 'ms');
        }
        $('.options').children('li').eq(1).css('transform', 'translateY(' + 2 + 'px)');
        $('.options').children('li').eq(2).css('transform', 'translateY(' + 4 + 'px)');
    }

    // click event functions
    $('.selectLabel').click(function () {
        $(this).toggleClass('active');
        if ($(this).hasClass('active')) {
            openList();
            focusItems();
        }
        else {
            closeList();
        }
    });

    $(".options li").on('keypress click', function (e) {
        e.preventDefault();
        $('.options li').siblings().removeClass();
        closeList();
        $('.selectLabel').removeClass('active');
        $('.selectLabel').text($(this).text());
        defaultselectbox.val($(this).text());
        $('.selected-item p span').text($('.selectLabel').text());
    });

});

function focusItems() {

    $('.options').on('focus', 'li', function () {
        $this = $(this);
        $this.addClass('active').siblings().removeClass();
    }).on('keydown', 'li', function (e) {
        $this = $(this);
        if (e.keyCode == 40) {
            $this.next().focus();
            return false;
        } else if (e.keyCode == 38) {
            $this.prev().focus();
            return false;
        }
    }).find('li').first().focus();

}



var from_$input = $('#input_from').pickadate(),
    from_picker = from_$input.pickadate('picker')



// Check if there’s a “from” or “to” date to start with.
if (from_picker.get('value')) {
    to_picker.set('min', from_picker.get('select'))
}
if (to_picker.get('value')) {
    from_picker.set('max', to_picker.get('select'))
}

// When something is selected, update the “from” and “to” limits.
from_picker.on('set', function (event) {
    if (event.select) {
        to_picker.set('min', from_picker.get('select'))
    }
    else if ('clear' in event) {
        to_picker.set('min', false)
    }
})
to_picker.on('set', function (event) {
    if (event.select) {
        from_picker.set('max', to_picker.get('select'))
    }
    else if ('clear' in event) {
        from_picker.set('max', false)
    }
});



