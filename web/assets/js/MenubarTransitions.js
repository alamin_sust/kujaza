﻿var offset_stuck = 100;
var offset_show_down = 180;
jQuery(window).scroll(function () {
    if (jQuery(this).scrollTop() > offset_stuck) {
        jQuery('#navigation').addClass('navbar-fixed-top');
        jQuery('#navigation').addClass('shrink');
        jQuery('#navigation').removeClass('navbar-static-top');
        // …
    } else {
        jQuery('#navigation').removeClass('navbar-fixed-top');
        jQuery('#navigation').removeClass('shrink');
        jQuery('#navigation').addClass('navbar-static-top');
        // …
    }
    if (jQuery(this).scrollTop() > offset_show_down) {
        jQuery('#navigation').addClass('navbar-show-down');
    } else {
        jQuery('#navigation').removeClass('navbar-show-down');
    }
    if (jQuery(this).scrollTop() > offset_stuck) {
        jQuery('body').addClass('navbar-stuck');
        // …
    } else {
        jQuery('body').removeClass('navbar-stuck');
        // …
    }
});

//#navigation.navbar-fixed-top { 
//top:-100px;
//transition:1.5s;
//}
//.shrink.navbar{transition:1s; opacity:0.88; color:white; font-size:10pt;}
//body.navbar-stuck {
//    padding-top: 100px;
//}
//#navigation.navbar-show-down {
//    top: 0;
//}


//<nav id="navigation" class="navbar navbar-default">
//  <div class="container-fluid">
//    <div class="navbar-header navbar-right">
//      <button type="button" class="navbar-toggle" data-toggle="collapse" onclick="ToggleMenu(this)" data-target="#MenuBar">
//        <span class="icon-bar" id="bar1"></span>
//        <span class="icon-bar" id="bar2"></span>
//        <span class="icon-bar" id="bar3"></span>
//      </button>
//      <a class="navbar-brand" href="#">ايجل</a>
//    </div>
//    <div class="collapse navbar-collapse navbar-right" id="MenuBar">
//      <ul class="topMenu nav navbar-nav">
//       <li><a href="#">اتصلوا بنا</a></li>
//        <li><a href="#">من نحن</a></li>
//        <li><a href="#">خدماتنا</a></li>
//        <li><a href="#">الرئيسية</a></li>
//      </ul>
//    </div>
//  </div>
//</nav>