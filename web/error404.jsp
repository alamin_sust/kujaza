﻿<!DOCTYPE html>
<html>
<head>
    <title>Error 404 &bull; KUJAZA</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta charset="utf-8" />
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/main.css" rel="stylesheet" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
    <link href="assets/css/animate.css" rel="stylesheet" />
</head>
<body>
    <div class="container-fluid errorPage">
        <img class="img-responsive center-block" src="assets/img/logo.png" />
        <h1>Opppss..! Error 404</h1>
        <h2>This page may have been removed</h2>
    </div>
<center>
    <a class="aksBtn errorbtn this" href="#">Go to Homepage <span class="fa fa-angle-right"></span></a>
</center>    <!--<a href="#" class="errorlnk">Go to Homepage</a>-->
    <script src="assets/js/jquery-3.1.1.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
</body>
</html>
