﻿//<a href="#" class="backToTop disp-none"><span class="fa fa-arrow-up"></span></a>

$(document).ready(function () {

    //Check to see if the window is top if not then display button
    $(window).scroll(function () {
        if ($(this).scrollTop() > 250) {
            $('.backToTop').removeClass("animated slideOutRight disp-none");
            $('.backToTop').addClass("animated slideInRight");
        } else {
            $('.backToTop').removeClass("animated slideInRight");
            $('.backToTop').addClass("animated slideOutRight");
        }
    });

    //Click event to scroll to top
    $('.backToTop').click(function () {
        $('html, body').animate({ scrollTop: 0 }, 800);
        return false;
    });

});

/*

.disp-none
{
    display:none;
    visibility:hidden;
}

.backToTop {
    width: 60px;
    height: 60px;
    text-align: center;
    color: #fff;
    background-color: rgb(33, 150, 243);
    line-height: 60px;
    position: fixed;
    font-size:140%;
    margin-top: 43%;
    margin-left: calc(100% - 60px);
    z-index: 99999;
    visibility: visible;
    border:1px solid rgb(33, 150, 243);
}

.backToTop:hover,.backToTop:active,.backToTop:focus
{
    text-decoration:none;
    color:#fff;
}

.backToTop:hover span
{
    transition:0.4s;
    transform:scale(1.2);
}

.backToTop span
{
    transition:0.4s;
}


*/