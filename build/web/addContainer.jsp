<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="com.kujaza.connection.Database"%>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">
        <%@ include file="lang.jsp" %>
        <title><%=tr[lang][0]%> &bull; KUJAZA</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta charset="utf-8" />
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
        <link href="assets/css/main.css" rel="stylesheet" />
        <link href="assets/css/animate.css" rel="stylesheet" />
        <link href="assets/css/aos.css" rel="stylesheet" />
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
        <link href="assets/css/tabs.css" rel="stylesheet" />
        <link href="assets/css/tabstyles.css" rel="stylesheet" />
        <link href="assets/css/DatePicker.css" rel="stylesheet" />
    </head>
    <body class="bodybg">
        <%
            if (session.getAttribute("email") == null) {
                response.sendRedirect("login.jsp");
            }
            
            
            session.setAttribute("successMsg", null);
            Database db = new Database();
            db.connect();

            //try {

                Statement st0 = db.connection.createStatement();
                String q0 = "select id from shipper where email='" + session.getAttribute("email") + "'";
                ResultSet rs0 = st0.executeQuery(q0);
                rs0.next();
                String id = rs0.getString("id");

                Statement st = db.connection.createStatement();
                String q = "";

                String serviceName = "";
                String serviceDescription = "";
                String departure = "";
                String arrival = "";
                String shippingDate = "";
                String arrivalDate = "";
                String price = "";
                String volume = "";

                if (request.getParameter("serviceName") != null) {
                    serviceName = request.getParameter("serviceName");
                }
                if (request.getParameter("serviceDescription") != null) {
                    serviceDescription = request.getParameter("serviceDescription");
                }
                if (request.getParameter("departure") != null) {
                    departure = request.getParameter("departure");
                }
                if (request.getParameter("arrival") != null) {
                    arrival = request.getParameter("arrival");
                }
                if (request.getParameter("shippingDate") != null) {
                    shippingDate = request.getParameter("shippingDate");
                }
                if (request.getParameter("arrivalDate") != null) {
                    arrivalDate = request.getParameter("arrivalDate");
                }
                if (request.getParameter("price") != null) {
                    price = request.getParameter("price");
                }
                if (request.getParameter("volume") != null) {
                    volume = request.getParameter("volume");
                }

                if (!serviceName.isEmpty()) {
                    q = "insert into container (name,description,departure,arrival,shipping_date,arrival_date,price_per_m3,volume_m3,shipper_id) values ('"
                            + serviceName + "','" + serviceDescription + "','" + departure + "','" + arrival + "','" + shippingDate + "','" + arrivalDate + "'," + price + "," + volume + "," + id + ")";

                    st.executeUpdate(q);

                    session.setAttribute("successMsg", "Successfully Added Container.");
                    response.sendRedirect("containerList.jsp?sId="+id);
                }
                
                Statement stC = db.connection.createStatement();
                String qC = "";
                ResultSet rsC = null;
                
                qC = "select * from cities  order by city";
                rsC=stC.executeQuery(qC);

        %>

        <div>

            <!-- HEADER -->
            <%@ include file="header2.jsp" %>
            <!-- END HEADER -->


            <div class="addContainerForm">
                <%if (session.getAttribute("successMsg") != null) {%>
                <div class="alert alert-success">
                    <%=session.getAttribute("successMsg")%>
                </div>
                <%}%>
                <%if (session.getAttribute("failureMsg") != null) {%>
                <div class="alert alert-danger">
                    <%=session.getAttribute("failureMsg")%>
                </div>
                <%}%>
                <h2 style="font-size: 30px; text-align: center" ><b><%=tr[lang][1]%></b></h2>
                <section class="martop2">
                    <form action="addContainer.jsp" method="get">
                        <div class="tabs tabs-style-linetriangle">
                            <nav>
                                <ul>
                                    <li><a href="#section-linetriangle-1"><span><%=tr[lang][2]%></span></a></li>
                                    <li><a href="#section-linetriangle-2"><span><%=tr[lang][3]%></span></a></li>
                                </ul>
                            </nav>
                            <div class="content-wrap">
                                <section id="section-linetriangle-1">
                                    <table class="table-responsive">
                                        <tr>
                                            <td><%=tr[lang][4]%></td>
                                            <td>
                                                <input type="text" style="width:100%; min-width: 100px;" name="serviceName" maxlength="80" class="form-control" required="required" placeholder="<%=tr[lang][229]%>" data-toggle="tooltip" data-placement="right" 
                                                       title="<%=tr[lang][222]%>"/>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td><%=tr[lang][5]%></td>
                                            <td>
                                                <textarea class="form-control" style="width:100%; min-width: 100px;" name="serviceDescription" maxlength="1000" required="required" data-toggle="tooltip" data-placement="right" 
                                                          title="<%=tr[lang][223]%>" placeholder="<%=tr[lang][230]%>" id="txtServiceDescription"></textarea>
                                            </td>
                                        </tr>
                                    </table>    
                                </section>
                                <section id="section-linetriangle-2">
                                    <table class="table-responsive">
                                        <tr>
                                            <td><%=tr[lang][6]%></td>
                                            <td>
                                                <select name="departure" class="form-control" required="">
                                                    <option value=""><%=tr[lang][201]%></option>

                                                    <%
                                                    while(rsC.next()) {
                                                    %>
                                                    <option value="<%=rsC.getString("city")%>, <%=rsC.getString("country")%>"><%=rsC.getString("city")%>, <%=rsC.getString("country")%></option>
                                                    <%}%>
                                                </select>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td><%=tr[lang][7]%></td>
                                            <td>
                                                <select name="arrival" class="form-control" required="">
                                                    <option value=""><%=tr[lang][202]%></option>
                                                    <%
                                                        rsC.beforeFirst();
                                                    while(rsC.next()) {
                                                    %>
                                                    <option value="<%=rsC.getString("city")%>, <%=rsC.getString("country")%>"><%=rsC.getString("city")%>, <%=rsC.getString("country")%></option>
                                                    <%}%>
                                                </select>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td><%=tr[lang][8]%></td>
                                            <td>
                                                <input id="txtDepartureDate" type="text" style="width:100%;" name="shippingDate" maxlength="10" class="form-control" required="required" placeholder="<%=tr[lang][233]%>" data-toggle="tooltip" data-placement="right" 
                                                       title="<%=tr[lang][226]%>"/>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td><%=tr[lang][9]%></td>
                                            <td>
                                                <input type="text" id="txtArrivalDate" style="width:100%;" maxlength="10" name="arrivalDate" class="form-control" required="required" placeholder="<%=tr[lang][234]%>" data-toggle="tooltip" data-placement="right" 
                                                       title="<%=tr[lang][227]%>"/>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td><%=tr[lang][10]%><sup>3</sup></td>
                                            <td>
                                                <input type="number" style="width:100%;" min="5" maxlength="4" name="price" class="form-control" required="required" placeholder="<%=tr[lang][235]%>" data-toggle="tooltip" data-placement="right" 
                                                       title="<%=tr[lang][228]%>"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><%=tr[lang][28]%></td>
                                            <td>
                                                <input type="number" style="width:100%;" maxlength="4" name="volume" class="form-control" required="required" placeholder="<%=tr[lang][28]%>" data-toggle="tooltip" data-placement="right" 
                                                       title="<%=tr[lang][28]%>"/>
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                    <button type="submit" class="aksBtn"><%=tr[lang][11]%> <span class="fa fa-plus"></span></button>
                                </section>
                            </div><!-- /content -->
                        </div><!-- /tabs -->
                    </form>
                </section>
            </div>

            <!-- Modals -->
            <div id="SearchForm" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title"><%=tr[lang][12]%> <span class="fa fa-search"></span></h4>
                        </div>
                        <div class="modal-body">
                            <h2><%=tr[lang][13]%></h2>
                            <div class="stepwizard col-md-offset-3">
                                <div class="stepwizard-row setup-panel">
                                    <div class="stepwizard-step">
                                        <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
                                        <p><%=tr[lang][14]%></p>
                                    </div>
                                    <div class="stepwizard-step">
                                        <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                                        <p><%=tr[lang][15]%></p>
                                    </div>
                                    <div class="stepwizard-step">
                                        <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                                        <p><%=tr[lang][16]%></p>
                                    </div>
                                </div>
                            </div>

                            <div class="row setup-content" id="step-1">
                                <span class="input input--kozakura">
                                    <input class="input__field input__field--kozakura" type="text" id="input-7" />
                                    <label class="input__label input__label--kozakura" for="input-7">
                                        <span class="input__label-content input__label-content--kozakura" data-content="Departure from"><%=tr[lang][17]%></span>
                                    </label>
                                    <svg class="graphic graphic--kozakura" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                                        <path d="M1200,9c0,0-305.005,0-401.001,0C733,9,675.327,4.969,598,4.969C514.994,4.969,449.336,9,400.333,9C299.666,9,0,9,0,9v43c0,0,299.666,0,400.333,0c49.002,0,114.66,3.484,197.667,3.484c77.327,0,135-3.484,200.999-3.484C894.995,52,1200,52,1200,52V9z"/>
                                    </svg>
                                </span>

                                <span class="input input--kozakura">
                                    <input class="input__field input__field--kozakura" type="text" id="input-7" />
                                    <label class="input__label input__label--kozakura" for="input-7">
                                        <span class="input__label-content input__label-content--kozakura" data-content="Arrival to"><%=tr[lang][18]%></span>
                                    </label>
                                    <svg class="graphic graphic--kozakura" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                                        <path d="M1200,9c0,0-305.005,0-401.001,0C733,9,675.327,4.969,598,4.969C514.994,4.969,449.336,9,400.333,9C299.666,9,0,9,0,9v43c0,0,299.666,0,400.333,0c49.002,0,114.66,3.484,197.667,3.484c77.327,0,135-3.484,200.999-3.484C894.995,52,1200,52,1200,52V9z"/>
                                    </svg>
                                </span>
                                <button class="aksBtn nextBtn" type="button" ><span class="fa fa-arrow-right"></span></button>
                            </div>
                            <div class="row setup-content" id="step-2">
                                <select name="" id="cusSelectbox">
                                    <option value="Cubic"><%=tr[lang][19]%></option>
                                    <option value="Cylindrical"><%=tr[lang][20]%></option>
                                    <option value="Car"><%=tr[lang][21]%></option>
                                </select>

                                <span class="input input--kozakura">
                                    <input class="input__field input__field--kozakura" type="text" id="input-7" />
                                    <label class="input__label input__label--kozakura" for="input-7">
                                        <span class="input__label-content input__label-content--kozakura" data-content="Size"><%=tr[lang][22]%></span>
                                    </label>
                                    <svg class="graphic graphic--kozakura" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                                        <path d="M1200,9c0,0-305.005,0-401.001,0C733,9,675.327,4.969,598,4.969C514.994,4.969,449.336,9,400.333,9C299.666,9,0,9,0,9v43c0,0,299.666,0,400.333,0c49.002,0,114.66,3.484,197.667,3.484c77.327,0,135-3.484,200.999-3.484C894.995,52,1200,52,1200,52V9z"/>
                                    </svg>
                                </span>
                                <button class="aksBtn nextBtn" type="button" ><span class="fa fa-arrow-right"></span></button>
                            </div>
                            <div class="row setup-content" id="step-3">
                                <div class="col-xs-6 col-md-offset-3">
                                    <div class="col-md-12">
                                        <!--<span class="input input--kozakura">
                                                                <input id="theInput" name="datetimepicker" class="input__field input__field--kozakura" type="text" id="input-7" />
                                                                <label class="input__label input__label--kozakura" for="input-7">
                                                                        <span class="input__label-content input__label-content--kozakura" data-content="Ship Date">Ship Date</span>
                                                                </label>
                                                                <svg class="graphic graphic--kozakura" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                                                                        <path d="M1200,9c0,0-305.005,0-401.001,0C733,9,675.327,4.969,598,4.969C514.994,4.969,449.336,9,400.333,9C299.666,9,0,9,0,9v43c0,0,299.666,0,400.333,0c49.002,0,114.66,3.484,197.667,3.484c77.327,0,135-3.484,200.999-3.484C894.995,52,1200,52,1200,52V9z"/>
                                                                </svg>
                                                        </span>-->
                                        <input type="text" placeholder="Ship Date" id="theInput" />
                                        <button class="aksBtn" type="submit"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Scripts -->
            <script src="assets/js/jquery-3.1.1.min.js"></script>
            <script src="assets/js/bootstrap.min.js"></script>
            <script src="assets/js/classie.js"></script>
            <script src="assets/js/KeepOpen.js"></script>
            <script src="assets/js/typed.min.js"></script>
            <script src="assets/js/aos.js"></script>
            <script>AOS.init();</script>
            <script src="assets/js/main.js"></script>
            <script src="assets/js/DatePicker.js"></script>
            <script src="assets/js/cbpFWTabs.js"></script>
            <script>
                (function () {

                    [].slice.call(document.querySelectorAll('.tabs')).forEach(function (el) {
                        new CBPFWTabs(el);
                    });

                })();

                $('.acc-type').click(function () {
                    $('.acc-type').removeClass('this-type');
                    $('.acc-type-chkd').removeAttr('checked');
                    $(this).addClass('this-type');
                    $(this).next('.acc-type-chkd').attr('checked', 'checked');
                });

                $(document).ready(function () {
                    $('[data-toggle="tooltip"]').tooltip();
                });
            </script>
        </div>
        <% // } catch (Exception e) {
            //    System.out.println(e);
           // } finally {
           //     db.close();
          //  }
        %>
    </body>
</html>
