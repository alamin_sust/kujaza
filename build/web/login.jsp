<%@page import="com.kijaza.mail.EmailUtil"%>
<%@page import="java.util.Random"%>
<%@page import="javax.mail.MessagingException"%>
<%@page import="javax.mail.Transport"%>
<%@page import="javax.mail.Message"%>
<%@page import="javax.mail.internet.InternetAddress"%>
<%@page import="javax.mail.internet.MimeMessage"%>
<%@page import="javax.mail.Session"%>
<%@page import="java.util.Properties"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="com.kujaza.connection.Database"%>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">
        <%@ include file="lang.jsp" %>
        <title><%=tr[lang][121]%> &bull; KUJAZA</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta charset="utf-8" />
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
        <link href="assets/css/main.css" rel="stylesheet" />
        <link href="assets/css/animate.css" rel="stylesheet" />
        <link href="assets/css/aos.css" rel="stylesheet" />
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
        <link href="assets/css/tabs.css" rel="stylesheet" />
        <link href="assets/css/tabstyles.css" rel="stylesheet" />
    </head>
    <body class="bg1">



        <%
            Database db = new Database();
            db.connect();
            
            
            
            
            String recoveredPassword = null;
            session.setAttribute("successMsg", null);
            session.setAttribute("failureMsg", null);
            session.setAttribute("email", null);
            session.setAttribute("name", null);
            session.setAttribute("service", null);

            Statement st0 = db.connection.createStatement();
            Statement st1 = db.connection.createStatement();
            Statement st2 = db.connection.createStatement();
            Statement stAlter = db.connection.createStatement();
            String q0 = "";
            String q1 = "";
            String q2 = "";
            String qAlter = "";
            ResultSet rs0;
            ResultSet rs1;
            ResultSet rs2;
            ResultSet rsAlter;

            String name = "";
            String email = "";
            String password = "";
            String password2 = "";
            String type = "";
            String service = "";
            
            if (request.getParameter("key") != null && !request.getParameter("key").equals("") 
                    && request.getParameter("email") != null && !request.getParameter("email").equals("")
                    && request.getParameter("lang") != null && !request.getParameter("lang").equals("")) {
                Statement stA = db.connection.createStatement();
                String qA = "select count(*) as cnt from customer where email='"+request.getParameter("email")+"' and veification_key='"+request.getParameter("key")+"'";
                ResultSet rsA = stA.executeQuery(qA);
                rsA.next();
                if(rsA.getString("cnt").equals("1")) {
                Statement stU1 = db.connection.createStatement();
                String qU1 = "update customer set is_verified=1 where email='"+request.getParameter("email")+"'";
                stU1.executeUpdate(qU1);
                Statement stU2 = db.connection.createStatement();
                String qU2 = "update shipper set is_verified=1 where email='"+request.getParameter("email")+"'";
                stU2.executeUpdate(qU2);
                session.setAttribute("successMsg", "Successfully Activated!");
                
                } else {
                    session.setAttribute("failureMsg", "Error in activation!. Please try again.");
                }
            }
            
            if (request.getParameter("name") != null) {
                name = request.getParameter("name");
            }
            if (request.getParameter("email") != null) {
                email = request.getParameter("email");
            }
            if (request.getParameter("password") != null) {
                password = request.getParameter("password");
            }
            if (request.getParameter("password2") != null) {
                password2 = request.getParameter("password2");
            }
            if (request.getParameter("type") != null) {
                type = request.getParameter("type");
            }
            if (request.getParameter("service") != null) {
                service = request.getParameter("service");
            }

            if (type.equals("register")) {

                Random random = new Random();
                int key = Math.abs(random.nextInt()) % 100000000;
                
                if (service.equals("shipper")) {
                    q0 = "select count(*) as cnt from customer where email='" + email + "'";
                    rs0 = st0.executeQuery(q0);
                    rs0.next();
                    q1 = "select count(*) as cnt from shipper where email='" + email + "'";
                    rs1 = st1.executeQuery(q1);
                    rs1.next();
                    if (rs1.getInt("cnt") == 0 && rs0.getInt("cnt") == 0) {
                        if (!password.equals(password2)) {
                            session.setAttribute("failureMsg", tr[lang][208]);
                        } else {
                            
                            
                            
                            q2 = "insert into shipper (name,email,password,member_since,question1,answer1,question2,answer2,veification_key,is_verified) values ('" + name + "','" + email + "','" + password + "',now(),'" + request.getParameter("question1") + "','" + request.getParameter("answer1") + "','" + request.getParameter("question2") + "','" + request.getParameter("answer2") + "','"+key+"',0)";
                            st2.executeUpdate(q2);
                            qAlter = "insert into customer (name,email,password,question1,answer1,question2,answer2,veification_key,is_verified) values ('" + name + "','" + email + "','" + password + "','" + request.getParameter("question1") + "','" + request.getParameter("answer1") + "','" + request.getParameter("question2") + "','" + request.getParameter("answer2") + "','"+key+"',0)";
                            stAlter.executeUpdate(qAlter);
                            EmailUtil.sendVerificationEmail(email, Integer.toString(key), lang);
                            if(lang==0) {
                                session.setAttribute("successMsg", "Congratulations! You are succesfully registered as: " + email + ". A verication email sent. Please verify your email to activate your account");
                            } else {
                                session.setAttribute("successMsg", "F�licitations! Vous �tes enregistr� avec succ�s en tant que: "+email+". Un email de confirmation vous a �t� envoy�. Veuillez v�rifier votre boite de r�ception pour activer votre compte.");
                            }
                            
                            

                        }
                    } else {
                        session.setAttribute("failureMsg", tr[lang][200]);
                    }

                } else {
                    q0 = "select count(*) as cnt from customer where email='" + email + "'";
                    rs0 = st0.executeQuery(q0);
                    rs0.next();
                    q1 = "select count(*) as cnt from shipper where email='" + email + "'";
                    rs1 = st1.executeQuery(q1);
                    rs1.next();
                    if (rs1.getInt("cnt") == 0 && rs0.getInt("cnt") == 0) {
                        if (!password.equals(password2)) {
                            session.setAttribute("failureMsg", tr[lang][210]);
                        } else {
                            q2 = "insert into customer (name,email,password,question1,answer1,question2,answer2,veification_key,is_verified) values ('" + name + "','" + email + "','" + password + "','" + request.getParameter("question1") + "','" + request.getParameter("answer1") + "','" + request.getParameter("question2") + "','" + request.getParameter("answer2") + "','"+key+"',0)";
                            st2.executeUpdate(q2);
                            qAlter = "insert into shipper (name,email,password,member_since,question1,answer1,question2,answer2,veification_key,is_verified) values ('" + name + "','" + email + "','" + password + "',now(),'" + request.getParameter("question1") + "','" + request.getParameter("answer1") + "','" + request.getParameter("question2") + "','" + request.getParameter("answer2") + "','"+key+"',0)";
                            stAlter.executeUpdate(qAlter);
                            EmailUtil.sendVerificationEmail(email, Integer.toString(key), lang);
                            
                            if(lang==0) {
                                session.setAttribute("successMsg", "Congratulations! You are succesfully registered as: " + email + ". A verication email sent. Please verify your email to activate your account");
                            } else {
                                session.setAttribute("successMsg", "F�licitations! Vous �tes enregistr� avec succ�s en tant que: "+email+". Un email de confirmation vous a �t� envoy�. Veuillez v�rifier votre boite de r�ception pour activer votre compte.");
                            }

                        }
                    } else {
                        session.setAttribute("failureMsg", tr[lang][195]);
                    }
                }

            } else if (type.equals("login")) {

                q0 = "select count(*) as cnt from customer where email='" + email + "' and is_verified=1";
                rs0 = st0.executeQuery(q0);
                rs0.next();
                q1 = "select count(*) as cnt from shipper where email='" + email + "' and is_verified=1";
                rs1 = st1.executeQuery(q1);
                rs1.next();
                if (rs1.getInt("cnt") == 0 && rs0.getInt("cnt") == 0) {
                    session.setAttribute("failureMsg", tr[lang][212]);
                } else {

                    if (rs1.getInt("cnt") > 0) {
                        q2 = "select * from shipper where email='" + email + "'";
                        rs2 = st2.executeQuery(q2);

                        while (rs2.next()) {
                            if (rs2.getString("password").equals(password)) {
                                session.setAttribute("successMsg", tr[lang][213] + email);
                                session.setAttribute("email", email);
                                session.setAttribute("name", rs2.getString("name"));
                                session.setAttribute("service", "shipper");
                                response.sendRedirect("index.jsp");
                            } else {
                                session.setAttribute("failureMsg", tr[lang][214]);
                            }
                        }

                    } else {
                        q2 = "select * from customer where email='" + email + "'";
                        rs2 = st2.executeQuery(q2);

                        while (rs2.next()) {
                            if (rs2.getString("password").equals(password)) {
                                session.setAttribute("successMsg", tr[lang][215] + email);
                                session.setAttribute("email", email);
                                session.setAttribute("name", rs2.getString("name"));
                                session.setAttribute("service", "customer");
                                response.sendRedirect("index.jsp");
                            } else {
                                session.setAttribute("failureMsg", tr[lang][216]);
                            }
                        }
                    }
                }

            } else if (type.equals("forgotPass")) {

                String ans1 = "";
                String ans2 = "";

                if (request.getParameter("userType").equals("customer")) {
                    Statement st5 = db.connection.createStatement();
                    String q5 = "select * from customer where email='" + request.getParameter("forgotPassUsername") + "'";
                    ResultSet rs5 = st5.executeQuery(q5);
                    rs5.next();
                    ans1 = rs5.getString("answer1");
                    ans2 = rs5.getString("answer2");
                    if(ans1.equals(request.getParameter("answer1")) && ans2.equals(request.getParameter("answer2"))) {
                        recoveredPassword = rs5.getString("password");
                    } else {
                        recoveredPassword="";
                    }
                } else {
                    Statement st5 = db.connection.createStatement();
                    String q5 = "select * from customer where email='" + request.getParameter("forgotPassUsername") + "'";
                    ResultSet rs5 = st5.executeQuery(q5);
                    rs5.next();
                    ans1 = rs5.getString("answer1");
                    ans2 = rs5.getString("answer2");
                    if(ans1.equals(request.getParameter("answer1")) && ans2.equals(request.getParameter("answer2"))) {
                    recoveredPassword = rs5.getString("password");
                    } else {
                        recoveredPassword="";
                    }
                }

//                String result;
//
//                // Recipient's email ID needs to be mentioned.
//                String to = email;
//
//                // Sender's email ID needs to be mentioned
//                String from = "md_alamin53@yahoo.co.uk";
//
//                // Assuming you are sending email from localhost
//                String host = "localhost";
//
//                // Get system properties object
//                Properties properties = System.getProperties();
//
//                // Setup mail server
//                properties.setProperty("mail.smtp.host", host);
//
//                // Get the default Session object.
//                Session mailSession = Session.getDefaultInstance(properties);
//
//                try {
//                    // Create a default MimeMessage object.
//                    MimeMessage message = new MimeMessage(mailSession);
//
//                    // Set From: header field of the header.
//                    message.setFrom(new InternetAddress(from));
//
//                    // Set To: header field of the header.
//                    message.addRecipient(Message.RecipientType.TO,
//                            new InternetAddress(to));
//                    // Set Subject: header field
//                    message.setSubject("This is the Subject Line!");
//
//                    // Now set the actual message
//                    message.setText("This is actual message");
//
//                    // Send message
//                    Transport.send(message);
//                    result = "Sent email successfully....";
//                    session.setAttribute("successMsg", result);
//                } catch (MessagingException mex) {
//                    mex.printStackTrace();
//                    result = "Error: unable to send email....";
//                    session.setAttribute("failureMsg", result);
//                    
//                }
            }
        %>


        <div class="formBox col-lg-6 col-md-8 col-sm-12 col-xs-12 col-lg-push-3 col-md-push-2">
            <img class="center-block img-responsive" src="assets/img/logo.png" />
            <%if (session.getAttribute("successMsg") != null) {%>
            <div class="alert alert-success">
                <%=session.getAttribute("successMsg")%>
            </div>
            <%}%>
            <%if (session.getAttribute("failureMsg") != null) {%>
            <div class="alert alert-danger">
                <%=session.getAttribute("failureMsg")%>
            </div>
            <%}%>


            <%if (recoveredPassword!=null && !recoveredPassword.equals("")) {%>
            <div class="alert alert-success"><%=tr[lang][217]%> <%=recoveredPassword%></div>
            <%} else if(recoveredPassword!=null && recoveredPassword.equals("")){%>
            <div class="alert alert-danger"><%=tr[lang][218]%> Try Again.</div>
            <%}%>
            
            <%if(request.getParameter("type")!=null && request.getParameter("type").equals("forgotPassPre")){%>
            <div class="alert alert-info"><b><%=tr[lang][199]%></b></div>
            <%}%>
            <div class="tabs tabs-style-underline">
                <nav>
                    <ul>
                        <li><a href="#section-underline-1"><span class="fa fa-2x fa-sign-in"></span> <%=tr[lang][122]%></a></li>
                        <li><a href="#section-underline-2"><span class="fa fa-user-plus fa-2x"></span> <%=tr[lang][123]%></a></li>
                        <li><a href="#section-underline-3"><span class="fa fa-history fa-2x"></span><%=tr[lang][124]%></a></li>
                    </ul>
                </nav>
                <div class="content-wrap">
                    <section id="section-underline-1">
                        
                        <p class="sec-title"><%=tr[lang][125]%></p>
                        <p class="sec-slog"><%=tr[lang][126]%></p>
                        <form type="submit" method="post" action="login.jsp">
                            <div class="row">
                                <span class="input input--kozakura">
                                    <input class="input__field input__field--kozakura" name="email" type="email" id="txtEmailLogin" />
                                    <label class="input__label input__label--kozakura" for="txtEmailLogin">
                                        <span class="input__label-content input__label-content--kozakura" data-content="<%=tr[lang][127]%>"><%=tr[lang][127]%></span>
                                    </label>
                                    <svg class="graphic graphic--kozakura" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                                        <path d="M1200,9c0,0-305.005,0-401.001,0C733,9,675.327,4.969,598,4.969C514.994,4.969,449.336,9,400.333,9C299.666,9,0,9,0,9v43c0,0,299.666,0,400.333,0c49.002,0,114.66,3.484,197.667,3.484c77.327,0,135-3.484,200.999-3.484C894.995,52,1200,52,1200,52V9z"/>
                                    </svg>
                                </span>
                            </div>

                            <div class="row">
                                <span class="input input--kozakura">
                                    <input class="input__field input__field--kozakura" name="password" type="password" id="txtPasswordLogin" />
                                    <label class="input__label input__label--kozakura" for="txtPasswordLogin">
                                        <span class="input__label-content input__label-content--kozakura" data-content="<%=tr[lang][128]%>"><%=tr[lang][128]%></span>
                                    </label>
                                    <svg class="graphic graphic--kozakura" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                                        <path d="M1200,9c0,0-305.005,0-401.001,0C733,9,675.327,4.969,598,4.969C514.994,4.969,449.336,9,400.333,9C299.666,9,0,9,0,9v43c0,0,299.666,0,400.333,0c49.002,0,114.66,3.484,197.667,3.484c77.327,0,135-3.484,200.999-3.484C894.995,52,1200,52,1200,52V9z"/>
                                    </svg>
                                </span>
                            </div>
                            <input type="hidden" name="type" value="login"/>
                            <button type="submit" class="aksBtn"><%=tr[lang][129]%> <span class="fa fa-angle-right"></span></button>
                        </form>
                    </section>


                    <section id="section-underline-2">
                        <h1 class="sec-title"><%=tr[lang][130]%></h1>
                        <p class="sec-slog"><%=tr[lang][131]%></p>

                        <form type="submit" method="post" action="login.jsp">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <span class="input input--kozakura">
                                        <input class="input__field input__field--kozakura" type="text" name="name" id="txtFullName" />
                                        <label class="input__label input__label--kozakura" for="txtFullName">
                                            <span class="input__label-content input__label-content--kozakura" data-content="Full Name"><%=tr[lang][132]%></span>
                                        </label>
                                        <svg class="graphic graphic--kozakura" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                                            <path d="M1200,9c0,0-305.005,0-401.001,0C733,9,675.327,4.969,598,4.969C514.994,4.969,449.336,9,400.333,9C299.666,9,0,9,0,9v43c0,0,299.666,0,400.333,0c49.002,0,114.66,3.484,197.667,3.484c77.327,0,135-3.484,200.999-3.484C894.995,52,1200,52,1200,52V9z"/>
                                        </svg>
                                    </span>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <span class="input input--kozakura">
                                        <input class="input__field input__field--kozakura" name="email" type="email" id="txtEmailRegister" />
                                        <label class="input__label input__label--kozakura" for="txtEmailRegister">
                                            <span class="input__label-content input__label-content--kozakura" data-content="Email"><%=tr[lang][133]%></span>
                                        </label>
                                        <svg class="graphic graphic--kozakura" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                                            <path d="M1200,9c0,0-305.005,0-401.001,0C733,9,675.327,4.969,598,4.969C514.994,4.969,449.336,9,400.333,9C299.666,9,0,9,0,9v43c0,0,299.666,0,400.333,0c49.002,0,114.66,3.484,197.667,3.484c77.327,0,135-3.484,200.999-3.484C894.995,52,1200,52,1200,52V9z"/>
                                        </svg>
                                    </span>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <span class="input input--kozakura">
                                        <input class="input__field input__field--kozakura" name="password" type="password" id="txtPasswordRegister" />
                                        <label class="input__label input__label--kozakura" for="txtFullName">
                                            <span class="input__label-content input__label-content--kozakura" data-content="<%=tr[lang][194]%>"><%=tr[lang][134]%></span>
                                        </label>
                                        <svg class="graphic graphic--kozakura" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                                            <path d="M1200,9c0,0-305.005,0-401.001,0C733,9,675.327,4.969,598,4.969C514.994,4.969,449.336,9,400.333,9C299.666,9,0,9,0,9v43c0,0,299.666,0,400.333,0c49.002,0,114.66,3.484,197.667,3.484c77.327,0,135-3.484,200.999-3.484C894.995,52,1200,52,1200,52V9z"/>
                                        </svg>
                                    </span>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <span class="input input--kozakura">
                                        <input class="input__field input__field--kozakura" name="password2" type="password" id="txtRetypePassword" />
                                        <label class="input__label input__label--kozakura" for="txtRetypePassword">
                                            <span class="input__label-content input__label-content--kozakura" data-content="<%=tr[lang][195]%>"><%=tr[lang][135]%></span>
                                        </label>
                                        <svg class="graphic graphic--kozakura" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                                            <path d="M1200,9c0,0-305.005,0-401.001,0C733,9,675.327,4.969,598,4.969C514.994,4.969,449.336,9,400.333,9C299.666,9,0,9,0,9v43c0,0,299.666,0,400.333,0c49.002,0,114.66,3.484,197.667,3.484c77.327,0,135-3.484,200.999-3.484C894.995,52,1200,52,1200,52V9z"/>
                                        </svg>
                                    </span>
                                </div>
                            </div>
                            <br><br>

                                    <legend style="text-align: center"><%=tr[lang][196]%></legend>
                                    <br>

                                        <div class="row">
                                            <div class="col-sm-5 col-sm-offset-1 col-xs-12">

                                                <select name="question1" required="" class="form-control">
                                                    <option value="">-- <%=tr[lang][197]%> --</option>
                                                    <option value="what was the first name of your best childhood friend?">
                                                        <%=tr[lang][204]%>
                                                    </option>
                                                    <option value="what was your mother birthplace?">
                                                        <%=tr[lang][205]%>
                                                    </option>
                                                </select>
                                            </div>
                                            <div class="col-sm-5 col-xs-12">
                                                <input type="text" name="answer1" class="form-control" placeholder="<%=tr[lang][198]%>" required=""/>
                                            </div>
                                        </div>

                                        <br>

                                            <div class="row">
                                                <div class="col-sm-5 col-sm-offset-1 col-xs-12">

                                                    <select name="question2" required="" class="form-control">
                                                        <option value="">-- <%=tr[lang][197]%> --</option>
                                                        <option value="what is your favourite sports team?">
                                                            <%=tr[lang][206]%>
                                                        </option>
                                                        <option value="who is your icon?">
                                                            <%=tr[lang][207]%>
                                                        </option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-5 col-xs-12">
                                                    <input type="text" name="answer2" class="form-control"  placeholder="<%=tr[lang][198]%>" required=""/>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="row acc-type-row">
                                                    <%=tr[lang][136]%>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="acc-type"><%=tr[lang][137]%> <span class="fa fa-dollar"></span></div>
                                                    <input type="checkbox" name="service" value="shipper" id="chkContainerOwner" class="hidden acc-type-chkd" />
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="acc-type this-type"><%=tr[lang][138]%> <span class="fa fa-cubes"></span></div>
                                                    <input type="checkbox" name="service" value="customer" id="chkHasPackage" class="hidden acc-type-chkd" checked="checked" />
                                                </div>
                                            </div>
                                            <br /><br />
                                            <input type="hidden" name="type" value="register"/>
                                            
                                            
                                            <!-- Trigger the modal with a button -->
<button type="button" class="aksBtn" data-toggle="modal" data-target="#myModal"><%=tr[lang][139]%> <span class="fa fa-check"></span></button>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Cookies Policy</h4>
      </div>
      <div class="modal-body">
          <%if(lang==0){%>
          <p style="color: black; font-size: large;">We use cookies to guarantee you the best experience on our site. If you continue to use it. We consider that you accept the use of cookies</p>
        <%}else{%>
        <p  style="color: black; font-size: large;">Nous utilisons des cookies pour vous garantir la meilleure exp�rience sur notre site. Si vous continuez � utiliser ce dernier. Nous consid�rons que vous acceptez l?utilisation des cookies.</p>
        <%}%>
      </div>
      <div class="modal-footer">
        <%--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--%>
        <button class="aksBtn" type="submit">Ok <span class="fa fa-check"></span></button>
      </div>
    </div>

  </div>
</div>
                                            
                                            
                                            
                                            
                                            
                                            </form>
                                            </section>

                                            <section id="section-underline-3">
                                                <%--
                                                <h1 class="sec-title"><%=tr[lang][140]%></h1>
                                                <p class="sec-slog"><%=tr[lang][141]%></p>
                                                --%>
                                                <%if (request.getParameter("forgotPassUsername") != null) {

                                                        Statement st3 = db.connection.createStatement();
                                                        String q3 = "select count(*) as cnt from customer where email='" + request.getParameter("forgotPassUsername") + "'";
                                                        ResultSet rs3 = st3.executeQuery(q3);
                                                        rs3.next();
                                                        Statement st4 = db.connection.createStatement();
                                                        String q4 = "select count(*) as cnt from shipper where email='" + request.getParameter("forgotPassUsername") + "'";
                                                        ResultSet rs4 = st4.executeQuery(q4);
                                                        rs4.next();

                                                        String ques1 = "";
                                                        String ques2 = "";
                                                        String userType = "";

                                                        if (rs3.getInt("cnt") == 1) {
                                                            Statement st5 = db.connection.createStatement();
                                                            String q5 = "select * from customer where email='" + request.getParameter("forgotPassUsername") + "'";
                                                            ResultSet rs5 = st5.executeQuery(q5);
                                                            rs5.next();
                                                            userType = "customer";
                                                            ques1 = rs5.getString("question1");
                                                            ques2 = rs5.getString("question2");
                                                            
                                                            if(ques1.contains("childhood")) {
                                                                ques1=tr[lang][204];
                                                            } else {
                                                                ques1=tr[lang][205];
                                                            }
                                                            if(ques2.contains("sports")) {
                                                                ques2=tr[lang][206];
                                                            } else {
                                                                ques2=tr[lang][207];
                                                            }
                                                            
                                                        } else if (rs4.getInt("cnt") == 1) {
                                                            Statement st5 = db.connection.createStatement();
                                                            String q5 = "select * from shipper where email='" + request.getParameter("forgotPassUsername") + "'";
                                                            ResultSet rs5 = st5.executeQuery(q5);
                                                            rs5.next();
                                                            userType = "shipper";
                                                            ques1 = rs5.getString("question1");
                                                            ques2 = rs5.getString("question2");
                                                            if(ques1.contains("childhood")) {
                                                                ques1=tr[lang][204];
                                                            } else {
                                                                ques1=tr[lang][205];
                                                            }
                                                            if(ques2.contains("sports")) {
                                                                ques2=tr[lang][206];
                                                            } else {
                                                                ques2=tr[lang][207];
                                                            }
                                                        }
                                                        
                                                        if (!userType.equals("")) {
                                                %>

                                                

                                                <form type="submit" method="post" action="login.jsp">
                                                    <legend style="text-align: center"><%=tr[lang][196]%></legend>
                                                    <br>

                                                        <div class="row">
                                                            <div class="col-sm-5 col-sm-offset-1 col-xs-12">
                                                                <label><%=ques1%></label>
                                                            </div>
                                                            <div class="col-sm-5 col-xs-12">
                                                                <input type="text" name="answer1" class="form-control" placeholder='<%=tr[lang][198]%>'/>
                                                            </div>
                                                        </div>

                                                        <br>

                                                            <div class="row">
                                                                <div class="col-sm-5 col-sm-offset-1 col-xs-12">

                                                                    <label><%=ques2%></label>
                                                                </div>
                                                                <div class="col-sm-5 col-xs-12">
                                                                    <input type="text" name="answer2" class="form-control"  placeholder='<%=tr[lang][198]%>'/>
                                                                </div>
                                                            </div>
                                                            <input type="hidden" name="type" value="forgotPass"/>
                                                            <input type="hidden" name="userType" value="<%=userType%>"/>
                                                            <input type="hidden" name="forgotPassUsername" value="<%=request.getParameter("forgotPassUsername")%>"/>

                                                            <button class="aksBtn"><%=tr[lang][193]%><span class="fa fa-envelope"></span></button>
                                                            </form>
                                                            <%} else {%>

                                                            <form type="submit" method="post" action="login.jsp">
                                                                <div class="row">
                                                                    <div class="alert alert-danger">Email Incorrect, Try Again.</div>
                                                                    <span class="input input--kozakura">
                                                                        <input class="input__field input__field--kozakura" name="forgotPassUsername" type="text" id="txtEmailForgotPassword" />
                                                                        <label class="input__label input__label--kozakura" for="txtEmailForgotPassword">
                                                                            <span class="input__label-content input__label-content--kozakura" data-content="<%=tr[lang][219]%>"><%=tr[lang][219]%></span>
                                                                        </label>
                                                                        <svg class="graphic graphic--kozakura" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                                                                            <path d="M1200,9c0,0-305.005,0-401.001,0C733,9,675.327,4.969,598,4.969C514.994,4.969,449.336,9,400.333,9C299.666,9,0,9,0,9v43c0,0,299.666,0,400.333,0c49.002,0,114.66,3.484,197.667,3.484c77.327,0,135-3.484,200.999-3.484C894.995,52,1200,52,1200,52V9z"/>
                                                                        </svg>
                                                                    </span>
                                                                </div>
                                                                <input type="hidden" name="type" value="forgotPassPre"/>
                                                                <button class="aksBtn" type="submit"><%=tr[lang][193]%><span class="fa fa-envelope"></span></button>
                                                            </form>

                                                            <%}
                                                            } else {


                                                            %>

                                                            <form type="submit" method="post" action="login.jsp">
                                                                <div class="row">
                                                                    <span class="input input--kozakura">
                                                                        <input class="input__field input__field--kozakura" name="forgotPassUsername" type="text" id="txtEmailForgotPassword" />
                                                                        <label class="input__label input__label--kozakura" for="txtEmailForgotPassword">
                                                                            <span class="input__label-content input__label-content--kozakura" data-content="">Email</span>
                                                                        </label>
                                                                        <svg class="graphic graphic--kozakura" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                                                                            <path d="M1200,9c0,0-305.005,0-401.001,0C733,9,675.327,4.969,598,4.969C514.994,4.969,449.336,9,400.333,9C299.666,9,0,9,0,9v43c0,0,299.666,0,400.333,0c49.002,0,114.66,3.484,197.667,3.484c77.327,0,135-3.484,200.999-3.484C894.995,52,1200,52,1200,52V9z"/>
                                                                        </svg>
                                                                    </span>
                                                                </div>
                                                                <input type="hidden" name="type" value="forgotPassPre"/>
                                                                <button class="aksBtn" type="submit"><%=tr[lang][193]%><span class="fa fa-envelope"></span></button>
                                                            </form>

                                                            <%                                                                }
                                                            %>
                                                            </section>
                                                            </div><!-- /content -->
                                                            </div><!-- /tabs -->
                                                            </div>
                                                            <!-- Scripts -->
                                                            <script src="assets/js/jquery-3.1.1.min.js"></script>
                                                            <script src="assets/js/bootstrap.min.js"></script>
                                                            <script src="assets/js/classie.js"></script>
                                                            <script src="assets/js/KeepOpen.js"></script>
                                                            <script src="assets/js/typed.min.js"></script>
                                                            <script src="assets/js/aos.js"></script>
                                                            <script>AOS.init();</script>
                                                            <script src="assets/js/main.js"></script>
                                                            <script src="assets/js/DatePicker.js"></script>
                                                            <script src="assets/js/cbpFWTabs.js"></script>
                                                            <script>
                                                    (function () {

                                                        [].slice.call(document.querySelectorAll('.tabs')).forEach(function (el) {
                                                            new CBPFWTabs(el);
                                                        });

                                                    })();

                                                    $('.acc-type').click(function () {
                                                        $('.acc-type').removeClass('this-type');
                                                        $('.acc-type-chkd').removeAttr('checked');
                                                        $(this).addClass('this-type');
                                                        $(this).next('.acc-type-chkd').attr('checked', 'checked');
                                                    });
                                                            </script>


                                                            <%

                                                            %>
                                                            </body>
                                                            </html>
