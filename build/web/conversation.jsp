<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="com.kujaza.connection.Database"%>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">
        <%@ include file="lang.jsp" %>
        <title><%=tr[lang][48]%>&bull; Kujaza &bull; <%=tr[lang][49]%></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta charset="utf-8" />
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
        <link href="assets/css/main.css" rel="stylesheet" />
        <link href="assets/css/animate.css" rel="stylesheet" />
        <link href="assets/css/aos.css" rel="stylesheet" />
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
        <link href="assets/css/DatePicker.css" rel="stylesheet" />
    </head>
    <body class="bodybg this">
        <%
            Database db = new Database();
            db.connect();

            //try{
            if (session.getAttribute("email") == null) {
                response.sendRedirect("index.jsp");
            }

            Statement st0 = db.connection.createStatement();
            Statement st1 = db.connection.createStatement();
            Statement st2 = db.connection.createStatement();
            String q0 = "";
            String q1 = "";
            String q2 = "";
            ResultSet rs0;
            ResultSet rs1;
            ResultSet rs2;


        %>
            <div>

                <!-- HEADER -->
                <%@ include file="header2.jsp" %>
                <!-- END HEADER -->

                <%            String email = session.getAttribute("email").toString();
                    String service = session.getAttribute("service").toString();

                    q0 = "select * from " + service + " where email='" + email + "'";
                    rs0 = st0.executeQuery(q0);
                    rs0.next();
                    String id = rs0.getString("id");
                    String name = rs0.getString("name");

                    if (request.getParameter("message") != null && !request.getParameter("message").equals("")) {

                        Statement stU = db.connection.createStatement();
                        String qU = "insert into conversation(sender_id,sender_type,receiver_id,receiver_type,message) values(" + id + ",'" + service + "'," + request.getParameter("idOther") + ",'" + (service.equals("customer") ? "shipper" : "customer") + "','" + request.getParameter("message") + "')";
                        stU.executeUpdate(qU);
                    }

                    if (service.equals("shipper")) {
                        q1 = "select * from conversation where (sender_id=" + id + " and sender_type='shipper') or (receiver_id=" + id + " and receiver_type='shipper')";
                    } else if (service.equals("customer")) {
                        q1 = "select * from conversation where (sender_id=" + id + " and sender_type='customer') or (receiver_id=" + id + " and receiver_type='customer')";
                    }

                    rs1 = st1.executeQuery(q1);


                %>

                <div class="container-fluid martop0">


                    <div class="row martop0">
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <div class="sellerInfo animated slideInLeft">
                                <p class="sellerInfoTitle" style="font-size: 30px;"><b><%=tr[lang][50]%></b></p>
                                <hr />
                                <%                           boolean mpp[] = new boolean[100010];
                                    while (rs1.next()) {
                                        String senderId = rs1.getString("sender_id");
                                        String receiverId = rs1.getString("receiver_id");
                                        String senderType = rs1.getString("sender_type");
                                        String receiverType = rs1.getString("receiver_type");
                                        String message = rs1.getString("message");
                                        String datetime = rs1.getString("datetime");

                                        boolean me = false;
                                        if (senderType.equals(service)) {
                                            me = true;
                                            if (service.equals("shipper")) {
                                                q2 = "select * from customer where id=" + receiverId;
                                            } else if (service.equals("customer")) {
                                                q2 = "select * from shipper where id=" + receiverId;
                                            }
                                        } else if (receiverType.equals(service)) {
                                            if (service.equals("shipper")) {
                                                q2 = "select * from customer where id=" + senderId;
                                            } else if (service.equals("customer")) {
                                                q2 = "select * from shipper where id=" + senderId;
                                            }
                                        }

                                        rs2 = st2.executeQuery(q2);
                                        rs2.next();
                                        String nameOther = rs2.getString("name");
                                        int idOther = rs2.getInt("id");

                                        if (mpp[idOther]) {
                                            continue;
                                        }
                                        mpp[idOther] = true;

                                        String src = "assets/img/";
                                        String srcOther = "assets/img/";

                                        if (service.equals("shipper")) {
                                            src += "avatar.png";
                                            srcOther += "avatarCustomer.png";
                                        } else if (service.equals("customer")) {
                                            src += "avatarCustomer.png";
                                            srcOther += "avatar.png";
                                        }

                                        String srcShow, nameShow;
                                        if (me) {
                                            srcShow = src;
                                            nameShow = name;
                                        } else {
                                            srcShow = srcOther;
                                            nameShow = nameOther;
                                        }


                                %> 

                                <tr>
                                    <td><img class="img-responsive img-circle center-block" style="width: 140px;height: 140px;" src="assets/img/users/<%=idOther%>.jpg" onclick="location.href = 'conversation.jsp?idOther=<%=idOther%>'"/></td>

                                    <%
                                        if (request.getParameter("idOther") != null && request.getParameter("idOther").equals(Integer.toString(idOther))) {
                                    %>
                                    <td style="font-size: larger; color: red;"><b><%=nameOther%></b></td>
                                            <%
                                            } else {

                                            %>
                                    <td><%=nameOther%></td>
                                    <%}%>
                                </tr>
                                <%
                                    }
                                    rs1.beforeFirst();
                                %>

                            </div>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                            <div class="todo-list animated slideInRight">
                                <p class="sellerInfoTitle" style="font-size: 30px;"><b><%=tr[lang][51]%></b></p>
                                <hr />
                                <table class="todo-tbl table-responsive">
                                    <%
                                        while (rs1.next()) {
                                            String senderId = rs1.getString("sender_id");
                                            String receiverId = rs1.getString("receiver_id");
                                            String senderType = rs1.getString("sender_type");
                                            String receiverType = rs1.getString("receiver_type");
                                            String message = rs1.getString("message");
                                            String datetime = rs1.getString("datetime");

                                            boolean me = false;
                                            if (senderType.equals(service)) {
                                                me = true;
                                                if (service.equals("shipper")) {
                                                    q2 = "select * from customer where id=" + receiverId;
                                                } else if (service.equals("customer")) {
                                                    q2 = "select * from shipper where id=" + receiverId;
                                                }
                                            } else if (receiverType.equals(service)) {
                                                if (service.equals("shipper")) {
                                                    q2 = "select * from customer where id=" + senderId;
                                                } else if (service.equals("customer")) {
                                                    q2 = "select * from shipper where id=" + senderId;
                                                }
                                            }

                                            rs2 = st2.executeQuery(q2);
                                            rs2.next();
                                            String nameOther = rs2.getString("name");
                                            String idOther = rs2.getString("id");

                                            if (request.getParameter("idOther") != null && !request.getParameter("idOther").equals("") && !idOther.equals(request.getParameter("idOther"))) {
                                                continue;
                                            }

                                            String src = "assets/img/";
                                            String srcOther = "assets/img/";

                                            if (service.equals("shipper")) {
                                                src += "avatar.png";
                                                srcOther += "avatarCustomer.png";
                                            } else if (service.equals("customer")) {
                                                src += "avatarCustomer.png";
                                                srcOther += "avatar.png";
                                            }

                                            String srcShow, nameShow;
                                            if (me) {
                                                srcShow = src;
                                                nameShow = name;
                                            } else {
                                                srcShow = srcOther;
                                                nameShow = nameOther;
                                            }

                                    %>

                                    <tr>
                                        <td><img class="img-responsive img-circle center-block" style="height: 100px; width: 100px;" src="assets/img/users/<%=senderId%>.jpg" /></td>
                                        <td><span><%=nameShow%></span>: <span><%=message%></span></td>
                                    </tr>
                                    <%

                                        }
                                    %>
                                    <%
                                        if (request.getParameter("idOther") != null && !request.getParameter("idOther").equals("")) {
                                    %>
                                    <tr>
                                        <form method="post" action="conversation.jsp" class="form-group">
                                            <td><input class="form-control" type="text" name="message" placeholder="Write your message here"/></td>
                                        <input type="hidden" name="idOther" value="<%=request.getParameter("idOther")%>"/>
                                        <td><button class="form-control btn btn-warning" type="submit"><%=tr[lang][52]%></button></td>
                                    </form>
                                    </tr>
                                        <%}%>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Scripts -->
                <script src="assets/js/jquery-3.1.1.min.js"></script>
                <script src="assets/js/bootstrap.min.js"></script>
                <script src="assets/js/classie.js"></script>
                <script src="assets/js/KeepOpen.js"></script>
                <script src="assets/js/typed.min.js"></script>
                <script src="assets/js/aos.js"></script>
                <script>AOS.init();</script>
                <script src="assets/js/main.js"></script>
                <script src="assets/js/DatePicker.js"></script>
            </div>
        <%
//        } catch (Exception e) {
//                System.out.println(e);
//            } finally {
//                db.close();
//            }
%>
    </body>
</html>
