<%@page import="java.sql.Statement"%>
<%@page import="com.kujaza.connection.Database"%>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <%@ include file="lang.jsp" %>
    <title><%=tr[lang][84]%> &bull; Kujaza &bull; <%=tr[lang][85]%></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta charset="utf-8" />
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/main.css" rel="stylesheet" />
    <link href="assets/css/animate.css" rel="stylesheet" />
    <link href="assets/css/aos.css" rel="stylesheet" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
    <link href="assets/css/DatePicker.css" rel="stylesheet" />
</head>
<body class="bodybg">
    
    <%
    if(session.getAttribute("email")==null) {
        response.sendRedirect("login.jsp");
    }
    session.setAttribute("successMsg", null);
    Database db =  new Database();
    db.connect();
    
    try {
    Statement st = db.connection.createStatement();
    String q="";
    
    String name = "";
    String email="";
    String reason = "";
    String description = "";
    
    if(request.getParameter("name")!=null) {
        name= request.getParameter("name");
    }
    if(request.getParameter("email")!=null) {
        email= request.getParameter("email");
    }
    if(request.getParameter("reason")!=null) {
        reason= request.getParameter("reason");
    }
    if(request.getParameter("description")!=null) {
        description= request.getParameter("description");
    }
    

    if(!name.isEmpty()) {
    q="insert into help (name,email,reason,description) values ('"+name+"','"+email+"','"+reason+"','"+description+"')";
    
    st.executeUpdate(q);
    
    session.setAttribute("successMsg", "Successfully Sent to Kujaza Support.");
    }

    



    

    
    
    %>
    
    <div>
        <!-- HEADER -->
    <%@ include file="header2.jsp" %>
    <!-- END HEADER -->

        <div class="container-fluid martop7">
            <%if (session.getAttribute("successMsg") != null) {%>
            <div class="alert alert-success">
                <%=session.getAttribute("successMsg")%>
            </div>
            <%}%>
            <%if (session.getAttribute("failureMsg") != null) {%>
            <div class="alert alert-danger">
                <%=session.getAttribute("failureMsg")%>
            </div>
            <%}%>
            <div class="row">
                <div class="aksBox">
                    <h1 class="help" style="font-size: 30px;"><b><%=tr[lang][86]%></b></h1>
                    <br />
                    <div class="panelx with-nav-tabs panelx-default">
                        <div class="panelx-heading">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#tab1default" data-toggle="tab"><%=tr[lang][87]%></a></li>
                                    <li><a href="#tab2default" data-toggle="tab"><%=tr[lang][88]%></a></li>
                                </ul>
                        </div>
                        <div class="panelx-body">
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="tab1default">
                                    <div class="accordion" id="accordion2">
                                                    <div class="section-even container-fluid">
                <div class="row">
                    <h2><%=tr[lang][271]%></h2>
                    <p><%=tr[lang][272]%></p>
                    <p><%=tr[lang][273]%></p>
                    <p><%=tr[lang][274]%></p>
                    <p><%=tr[lang][275]%></p>
                    <p><%=tr[lang][276]%></p>
                    <p><%=tr[lang][277]%></p>
                    <p><%=tr[lang][278]%></p>
                    <p><%=tr[lang][279]%></p>
                    <p><%=tr[lang][280]%></p>
                    <p><%=tr[lang][281]%></p>
                    <p><%=tr[lang][282]%></p>
                    <p><%=tr[lang][283]%></p>
                    
                    
                    
                    
                </div>
            </div>
                                        <div class="accordion-group">
                                            <div class="accordion-heading">
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
                                                    <%=tr[lang][89]%>
                                                </a>
                                            </div>
                                            <div id="collapseOne" class="accordion-body collapse">
                                                <div class="accordion-inner">
                                                    <%=tr[lang][90]%>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-group">
                                            <div class="accordion-heading">
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
                                                <%=tr[lang][91]%>
                                                </a>
                                            </div>
                                            <div id="collapseTwo" class="accordion-body collapse">
                                                <div class="accordion-inner">
                                                    <%=tr[lang][92]%>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-group">
                                            <div class="accordion-heading">
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">
                                                <%=tr[lang][93]%>
                                                </a>
                                            </div>
                                            <div id="collapseThree" class="accordion-body collapse">
                                                <div class="accordion-inner">
                                                    <%=tr[lang][94]%>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-group">
                                            <div class="accordion-heading">
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseFour">
                                                <%=tr[lang][95]%>
                                                </a>
                                            </div>
                                            <div id="collapseFour" class="accordion-body collapse">
                                                <div class="accordion-inner">
                                                    <%=tr[lang][96]%>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="tab2default">
                                    <form action="help.jsp" method="get">
                                    <table class="aksTbl">
                                        <tr>
                                            <td><label><%=tr[lang][97]%></label></td>
                                            <td><input type="text" name="name" class="form-control" required="required" /></td>
                                        </tr>
                                        <tr>
                                            <td><label><%=tr[lang][98]%></label></td>
                                            <td><input type="text" name="email" class="form-control" required="required" /></td>
                                        </tr>
                                        <tr>
                                            <td><label><%=tr[lang][99]%></label></td>
                                            <td><input type="text" name="reason" class="form-control" /></td>
                                        </tr>
                                        <tr>
                                            <td><label><%=tr[lang][100]%></label></td>
                                            <td>
                                                <textarea class="form-control" name="description" required="required">
                                                </textarea>
                                            </td>
                                        </tr>
                                    </table>
                                    <button class="aksBtn this"><%=tr[lang][101]%> <span class="fa fa-envelope"></span></button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            
           
        <!-- Scripts -->
        
    <script src="assets/js/jquery-3.1.1.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/classie.js"></script>
    <script src="assets/js/KeepOpen.js"></script>
    <script src="assets/js/typed.min.js"></script>
    <script src="assets/js/aos.js"></script>
    <script>AOS.init();</script>
    <script src="assets/js/main.js"></script>
        <script>
            $(document).on('show', '.accordion', function (e) {
                //$('.accordion-heading i').toggleClass(' ');
                $(e.target).prev('.accordion-heading').addClass('accordion-opened');
            });

            $(document).on('hide', '.accordion', function (e) {
                $(this).find('.accordion-heading').not($(e.target)).removeClass('accordion-opened');
                $('.accordion-heading i').toggleClass('fa-chevron-right fa-chevron-down');
            });
        </script>
    </div>
           <%  } catch (Exception e) {
                System.out.println(e);
            } finally {
                db.close();
            }
        %> 
</body>
</html>
