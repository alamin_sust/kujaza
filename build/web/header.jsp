<%@page import="com.kujaza.connection.Database"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta charset="utf-8" />
<link href="assets/css/bootstrap.min.css" rel="stylesheet" />
<link href="assets/css/main.css" rel="stylesheet" />
<link href="assets/css/animate.css" rel="stylesheet" />
<link href="assets/css/aos.css" rel="stylesheet" />
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
<link href="assets/css/DatePicker.css" rel="stylesheet" />



<nav id="MenuMain" class="navbar navbar-inverse" >
    <div class="container-fluid">
        <div class="navbar-header" >


            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span> 
            </button>
            
            
                <%if(session.getAttribute("lang")!=null){%>
                <a class="navbar-toggle" data-toggle="collapse" style="padding: 0" data-target="#myNavbar2" href="index.jsp?lang=1"> <img src="assets/img/fl-fr.png" style="width: 45%; height: 45%; margin-top: 7px; margin-bottom: 8px; margin-left: 8px;"  class="img-circle"/></a>
                                <%}else{%>
<a class="navbar-toggle" data-toggle="collapse" style="padding: 0" data-target="#myNavbar2" href="index.jsp?lang=0"> <img src="assets/img/fl-uk.png"  style="width: 45%; height: 45%; margin-top: 7px; margin-bottom: 8px; margin-left: 8px;" class="img-circle"/></a>
                                <%}%>
            
            
            <a class="" href="index.jsp">
                <img class="img-responsive animated zoomIn" style="margin-top: 5px; width: 90px; height: 40px;" src="assets/img/logo_white.png"  />
            </a>

        </div>
        
        <div class="collapse navbar-collapse" id="myNavbar" >
            <!--<ul class="nav navbar-nav">
              <li><a href="#">Home</a></li>
              <li><a href="#">Page 1</a></li>
              <li><a href="#">Page 2</a></li> 
              <li><a href="#">Page 3</a></li> 
            </ul>-->
            <ul class="nav navbar-nav navbar-right" style="padding-top: 5px; padding-bottom: 5px;">
                <%
                    
                    Database db = new Database();
            db.connect();
            
            
            
                    


                %>
<li id="myNavbar2"><%if(session.getAttribute("lang")!=null){%>
<a href="index.jsp?lang=1"> <img src="assets/img/fl-fr.png"  class="img-circle"/></a>
                                <%}else{%>
<a href="index.jsp?lang=0"> <img src="assets/img/fl-uk.png"  class="img-circle"/></a>
<%}%></li>
                                <%
                                if (session.getAttribute("service") != null && session.getAttribute("email") != null) {
                        Statement stH = db.connection.createStatement();
                        String qH = "select * from " + session.getAttribute("service") + " where email='" + session.getAttribute("email") + "'";
                        ResultSet rsH;
                        rsH = stH.executeQuery(qH);

                        rsH.next();
                    
            if(request.getParameter("switch")!=null && request.getParameter("switch").equals("shipper") && session.getAttribute("service") != null) {
                session.setAttribute("service", "shipper");
                response.sendRedirect("index.jsp");
            }
            if(request.getParameter("switch")!=null && request.getParameter("switch").equals("customer") && session.getAttribute("service") != null) {
                session.setAttribute("service", "customer");
                response.sendRedirect("index.jsp");
            }
                                
                                %>
                                
                                <li><a href="shipperProfile.jsp?sId=<%=rsH.getString("id")%>"><span class="fa fa-bar-chart"></span> <%=tr[lang][191]%></a></li>
                <li><a href="containerList.jsp?sId=<%=rsH.getString("id")%>"><span class="fa fa-bar-chart"></span> <%=tr[lang][192]%></a></li>
                <li><a href="dashBoard.jsp?sId=<%=rsH.getString("id")%>"><span class="fa fa-dashboard"></span> <%=tr[lang][188]%></a></li> 
                <%}%>
                <li><a href="addContainer.jsp" class="btn btn-success"><%=tr[lang][66]%> <span class="fa fa-plus"></span></a></li>
                        <%if (session.getAttribute("email") == null) {%>
                <li style="text-align: center;"><a href="login.jsp"><span class="fa fa-sign-in"></span> <%=tr[lang][67]%> / <span class="fa fa-user"></span> <%=tr[lang][68]%></a></li>
                    <%} else {%>
                
                <li style="text-align: center;"><a href="index.jsp"><%=tr[lang][69]%> <span class="fa fa-user"></span> <%=session.getAttribute("name")%></a></li>
                <%--
                <%if(session.getAttribute("service")!=null && session.getAttribute("service").toString().equals("shipper")) {%>
                <li style="text-align: center;"><a href="index.jsp?switch=customer"><%=tr[lang][220]%></a></li>
                <%} else {%>
                <li style="text-align: center;"><a href="index.jsp?switch=shipper"><%=tr[lang][221]%></a></li>
                <%}%>
                --%>
                <li style="text-align: center;"><a href="login.jsp"><%=tr[lang][70]%></a></li>
                    <%}%>
                
                
                <!--<li><a href="#"></a></li>-->
            </ul>
                    
        </div>
                                
                                
                                
                                
                                
                                
    </div>
</nav>
