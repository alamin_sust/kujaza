<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="com.kujaza.connection.Database"%>
<!DOCTYPE html>
<html>
<head>
    <%@ include file="lang.jsp" %>
    <title><%=tr[lang][23]%> &bull; KUJAZA</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta charset="utf-8" />
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/main.css" rel="stylesheet" />
    <link href="assets/css/animate.css" rel="stylesheet" />
    <link href="assets/css/aos.css" rel="stylesheet" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
    <link href="assets/css/tabs.css" rel="stylesheet" />
    <link href="assets/css/tabstyles.css" rel="stylesheet" />
    <link href="assets/css/DatePicker.css" rel="stylesheet" />
</head>
<body class="bodybg">
    <%
        
        Database db=new Database();
        db.connect();
        
    try{
       
        if(session.getAttribute("email")==null) {
            response.sendRedirect("index.jsp");
        }
        
    Statement st0 = db.connection.createStatement();
            Statement st1 = db.connection.createStatement();
            Statement st2 = db.connection.createStatement();
            String q0 = "";
            String q1 = "";
            String q2 = "";
            ResultSet rs0;
            ResultSet rs1;
            ResultSet rs2;

            String name = "";
            String description = "";
            String departure = "";
            String arrival = "";
            String volumeM3 = "";
            String shipperId = "";
            String pricePerM3 = "";
            String shippingDate = "";
            String arrivalDate = "";
            
            q0="select * from container where id="+request.getParameter("containerId");
            
            rs0=st0.executeQuery(q0);
            
            while(rs0.next()) {
                name = rs0.getString("name");
                description = rs0.getString("description");
                departure = rs0.getString("departure");
                arrival = rs0.getString("arrival");
                volumeM3 = rs0.getString("volume_m3");
                shipperId = rs0.getString("shipper_id");
                pricePerM3 = rs0.getString("price_per_m3");
                shippingDate = rs0.getString("shipping_date");
                arrivalDate = rs0.getString("arrival_date");
            }


    %>
    
    <!-- HEADER -->
    <%@ include file="header2.jsp" %>
    <!-- END HEADER -->

    <div class="container-fluid marg-menu">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                <div class="ContainerInfo animated fadeIn">
                    <h1 class="serviceTitle" style="font-size: 25px;"><b><%=name%></b></h1>
                    <hr />
                    <p class="serviceSubtitle martop7"><%=tr[lang][24]%></p>
                    <p id="lblContainerDescription">
                    <%=description%>    
                    </p>

                    <p class="serviceSubtitle martop7"><%=tr[lang][25]%></p>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <p class="serviceSubtitle2"><%=tr[lang][26]%></p>
                            <p class="aksBold" id="lblDepartureFromInGig"><%=departure%></p>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <p class="serviceSubtitle2"><%=tr[lang][27]%></p>
                            <p class="aksBold" id="lblShippingDateGig"><%=shippingDate%></p>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <p class="serviceSubtitle2"><%=tr[lang][28]%></p>
                            <p class="aksBold" id="lblAvailableVolume"><%=volumeM3%> m<sup>3</sup></p>
                        </div>
                    </div>
                    <div class="row martop2">
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <p class="serviceSubtitle2"><%=tr[lang][29]%></p>
                            <p class="aksBold" id="lblArrivalToGig"><%=arrival%></p>
                        </div>
                        <div class="col-lg-4 col-sm-4 col-md-4">
                            <p class="serviceSubtitle2"><%=tr[lang][30]%></p>
                            <p class="aksBold" id="lblArrivalDate"><%=arrivalDate%></p>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <p class="serviceSubtitle2"><%=tr[lang][31]%></p>
                            <p class="aksBold" id="lblPriceGig">$<%=pricePerM3%>/m<sup>3</sup></p>
                        </div>
                    </div>

                </div>
            </div>
                        <%
                        
                        q1="select * from shipper where id="+shipperId;
                        
                        rs1=st1.executeQuery(q1);
                        
                        String sId="";
                        String sName="";
                        String sImgUrl="";
                        String sRatingCount="";
                        String sTotalRatingPoint="";
                        String sOrderCount="";
                        String sMemberSince="";
                        String sAboutMe = "";
                        double sRating=0.0;
                        
                        while(rs1.next()) {
                            sId = rs1.getString("id");
                            sName = rs1.getString("name");
                            sImgUrl = rs1.getString("img_url");
                            sRatingCount = rs1.getString("rating_count");
                            sTotalRatingPoint = rs1.getString("total_rating_point");
                            sOrderCount = rs1.getString("order_count");
                            sMemberSince = rs1.getString("member_since");
                            sAboutMe = rs1.getString("about_me");
                            if(Double.parseDouble(sRatingCount)!=0.0) {
                                sRating=Double.parseDouble(sTotalRatingPoint)/Double.parseDouble(sRatingCount);
                            }
                            
                        }
                        
                        
                        
                        %>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="sellerInfo animated slideInRight">
                    <p class="sellerInfoTitle" style="font-size: 25px;"><b><%=tr[lang][32]%></b></p>
                    <hr />
                    <i class="martop7"></i>
                    <img class="img-responsive img-circle center-block" src="assets/img/users/<%=shipperId%>.jpg" />
                    <div class="row">
                        <label id="lblSellerName" class="center-block" style="font-size: 25px;"><b><%=sName%></b></label>
                        <p class="ReviewNumber"><span class="fa fa-star"></span><span><%=sRating%></span> (<%=sRatingCount%>)</p>
                    </div>
                    <div class="row">
                        <label class="aksLbl"><%=tr[lang][33]%> <span id="lblOrderCountGig"> <%=sOrderCount%></span></label>
                    </div>
                    <div class="row">
                        <label class="aksLbl"><%=tr[lang][34]%> <span id="lblMemberDate"> <%=sMemberSince%></span></label>
                    </div>
                    <div class="row form-inline">
                        <button class="aksBtn" onclick="location.href='shipperProfile.jsp?sId=<%=sId%>'"><%=tr[lang][35]%> <span class="fa fa-envelope"></span></button>
                        <button class="aksBtn" onclick="location.href='shipperProfile.jsp?sId=<%=sId%>'"><%=tr[lang][36]%> <span class="fa fa-id-badge"></span></button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modals -->
    <div id="SearchForm" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"><%=tr[lang][37]%> <span class="fa fa-search"></span></h4>
          </div>
          <div class="modal-body">
            <h2><%=tr[lang][38]%></h2>
            <div class="stepwizard col-md-offset-3">
                <div class="stepwizard-row setup-panel">
                  <div class="stepwizard-step">
                    <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
                    <p><%=tr[lang][39]%></p>
                  </div>
                  <div class="stepwizard-step">
                    <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                    <p><%=tr[lang][40]%></p>
                  </div>
                  <div class="stepwizard-step">
                    <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                    <p><%=tr[lang][41]%></p>
                  </div>
                </div>
              </div>
  
                <div class="row setup-content" id="step-1">
                      <span class="input input--kozakura">
					<input class="input__field input__field--kozakura" type="text" id="input-7" />
					<label class="input__label input__label--kozakura" for="input-7">
						<span class="input__label-content input__label-content--kozakura" data-content="Departure from"><%=tr[lang][42]%></span>
					</label>
					<svg class="graphic graphic--kozakura" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
						<path d="M1200,9c0,0-305.005,0-401.001,0C733,9,675.327,4.969,598,4.969C514.994,4.969,449.336,9,400.333,9C299.666,9,0,9,0,9v43c0,0,299.666,0,400.333,0c49.002,0,114.66,3.484,197.667,3.484c77.327,0,135-3.484,200.999-3.484C894.995,52,1200,52,1200,52V9z"/>
					</svg>
				</span>

            <span class="input input--kozakura">
					<input class="input__field input__field--kozakura" type="text" id="input-7" />
					<label class="input__label input__label--kozakura" for="input-7">
						<span class="input__label-content input__label-content--kozakura" data-content="Arrival to"><%=tr[lang][43]%></span>
					</label>
					<svg class="graphic graphic--kozakura" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
						<path d="M1200,9c0,0-305.005,0-401.001,0C733,9,675.327,4.969,598,4.969C514.994,4.969,449.336,9,400.333,9C299.666,9,0,9,0,9v43c0,0,299.666,0,400.333,0c49.002,0,114.66,3.484,197.667,3.484c77.327,0,135-3.484,200.999-3.484C894.995,52,1200,52,1200,52V9z"/>
					</svg>
				</span>
                      <button class="aksBtn nextBtn" type="button" ><span class="fa fa-arrow-right"></span></button>
                </div>
                <div class="row setup-content" id="step-2">
                      <select name="" id="cusSelectbox">
			            <option value="Cubic"><%=tr[lang][44]%></option>
			            <option value="Cylindrical"><%=tr[lang][45]%></option>
			            <option value="Car"><%=tr[lang][46]%></option>
		            </select>

            <span class="input input--kozakura">
					<input class="input__field input__field--kozakura" type="text" id="input-7" />
					<label class="input__label input__label--kozakura" for="input-7">
						<span class="input__label-content input__label-content--kozakura" data-content="Size"><%=tr[lang][47]%></span>
					</label>
					<svg class="graphic graphic--kozakura" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
						<path d="M1200,9c0,0-305.005,0-401.001,0C733,9,675.327,4.969,598,4.969C514.994,4.969,449.336,9,400.333,9C299.666,9,0,9,0,9v43c0,0,299.666,0,400.333,0c49.002,0,114.66,3.484,197.667,3.484c77.327,0,135-3.484,200.999-3.484C894.995,52,1200,52,1200,52V9z"/>
					</svg>
				</span>
                      <button class="aksBtn nextBtn" type="button" ><span class="fa fa-arrow-right"></span></button>
                </div>
                <div class="row setup-content" id="step-3">
                  <div class="col-xs-6 col-md-offset-3">
                    <div class="col-md-12">
                        <!--<span class="input input--kozakura">
					        <input id="theInput" name="datetimepicker" class="input__field input__field--kozakura" type="text" id="input-7" />
					        <label class="input__label input__label--kozakura" for="input-7">
						        <span class="input__label-content input__label-content--kozakura" data-content="Ship Date">Ship Date</span>
					        </label>
					        <svg class="graphic graphic--kozakura" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
						        <path d="M1200,9c0,0-305.005,0-401.001,0C733,9,675.327,4.969,598,4.969C514.994,4.969,449.336,9,400.333,9C299.666,9,0,9,0,9v43c0,0,299.666,0,400.333,0c49.002,0,114.66,3.484,197.667,3.484c77.327,0,135-3.484,200.999-3.484C894.995,52,1200,52,1200,52V9z"/>
					        </svg>
				        </span>-->
                        <input type="text" placeholder="Ship Date" id="theInput" />
                      <button class="aksBtn" type="submit"><i class="fa fa-search"></i></button>
                    </div>
                </div>
        </div>
          </div>
        </div>
          </div>
      </div>
    <!-- Scripts -->
    <script src="assets/js/jquery-3.1.1.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/classie.js"></script>
    <script src="assets/js/KeepOpen.js"></script>
    <script src="assets/js/typed.min.js"></script>
    <script src="assets/js/aos.js"></script>
    <script>AOS.init();</script>
    <script src="assets/js/main.js"></script>
    <script src="assets/js/DatePicker.js"></script>
        <script src="assets/js/cbpFWTabs.js"></script>
        <script>
			(function() {

				[].slice.call( document.querySelectorAll( '.tabs' ) ).forEach( function( el ) {
					new CBPFWTabs( el );
				});

			})();

			$('.acc-type').click(function () {
			    $('.acc-type').removeClass('this-type');
			    $('.acc-type-chkd').removeAttr('checked');
			    $(this).addClass('this-type');
			    $(this).next('.acc-type-chkd').attr('checked', 'checked');
			});
		</script>
                
           
        <%
        } catch (Exception e) {
                System.out.println(e);
            } finally {
                db.close();
            }
        %>
</body>
</html>
