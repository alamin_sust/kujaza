<%@page import="java.io.DataInputStream"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="com.kujaza.connection.Database"%>
<%@page import="java.io.OutputStream"%>
<%@page import="java.io.FileOutputStream"%>
<%@page import="java.io.File"%>
<%@page import="java.io.InputStream"%>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <%@ include file="lang.jsp" %>
    <title><%=tr[lang][157]%> &bull; KUJAZA</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta charset="utf-8" />
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/main.css" rel="stylesheet" />
    <link href="assets/css/animate.css" rel="stylesheet" />
    <link href="assets/css/aos.css" rel="stylesheet" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
    <link href="assets/css/tabs.css" rel="stylesheet" />
    <link href="assets/css/tabstyles.css" rel="stylesheet" />
    <link href="assets/css/DatePicker.css" rel="stylesheet" />
</head>
<body class="bodybg">
    
    <%
    
    Database db=new Database();
        db.connect();
        
    //try{
       
        if(session.getAttribute("email")==null) {
            response.sendRedirect("index.jsp");
        }
        
        String email=session.getAttribute("email").toString();
        String service=session.getAttribute("service").toString();
        
        
    Statement st0 = db.connection.createStatement();
            Statement st1 = db.connection.createStatement();
            Statement st2 = db.connection.createStatement();
            Statement st3 = db.connection.createStatement();
            String q0 = "";
            String q1 = "";
            String q2 = "";
            String q3 = "";
            ResultSet rs0;
            ResultSet rs1;
            ResultSet rs2;
            ResultSet rs3;
    
    
    
//            Part filePart = request.getPart("file"); // Retrieves <input type="file" name="file">
//                InputStream inStream = filePart.getInputStream();
//                File bfile =new File(getServletContext().getRealPath("/")+".jpg");
//                //out.print(getServletContext().getRealPath("/"));
//                 OutputStream outStream = new FileOutputStream(bfile);
//                 byte[] buffer = new byte[1024];
//    	    int length;
//    	    while ((length = inStream.read(buffer)) > 0){
//    	    	outStream.write(buffer, 0, length);
//    	    }
//    	    inStream.close();
//    	    outStream.close();

          q0="select * from "+service+" where email='"+email+"'";
          rs0=st0.executeQuery(q0);
          rs0.next();
          
          String id=rs0.getString("id");
          String name=rs0.getString("name");
          String description=rs0.getString("about_me");
          String password=rs0.getString("password");
          
          if(request.getParameter("email")!=null && !request.getParameter("email").equals("")) {
              email=request.getParameter("email");
          }
          if(request.getParameter("name")!=null && !request.getParameter("name").equals("")) {
              name=request.getParameter("name");
          }
          if(request.getParameter("description")!=null && !request.getParameter("description").equals("")) {
              description=request.getParameter("description");
          }
          if(request.getParameter("password")!=null && !request.getParameter("password").equals("")) {
              
              if(request.getParameter("password").equals(password) && request.getParameter("newPassword").equals(request.getParameter("newPassword2"))) {
                  password=request.getParameter("newPassword");
              }
          }


          q1="update "+service+" set name='"+name+"', email='"+email+"', about_me='"+description+"', password='"+password+"' where id="+id;

          st1.executeUpdate(q1);
          
          session.setAttribute("email", email);

    %>
    
    
    <div>
        
        
        <!-- HEADER -->
    <%@ include file="header2.jsp" %>
    <!-- END HEADER -->

        <div class="container martop7">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            <i class="more-less fa fa-minus"></i>
                            <%=tr[lang][158]%>
                        </a>
                    </h4>
                </div>
                
                <div id="collapseOne" class="panel-collapse in collapse" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">
                        <div class="container">
                            <table class="table-responsive aksTbl">
                                <tr>
                                    <td><%=tr[lang][159]%></td>
                                
                                <form method="POST" action="settings.jsp?is_image_selected=yes" enctype="multipart/form-data" style="width: 110px; padding-top: 5px; height: 65px;">
                                        <%
                                            String saveFile = new String();
                                            String saveFile2 = new String();
                                            String contentType = request.getContentType();
                                            if ((contentType != null) && (contentType.indexOf("multipart/form-data") >= 0)) {
                                                DataInputStream in = new DataInputStream(request.getInputStream());

                                                int formDataLength = request.getContentLength();
                                                byte dataBytes[] = new byte[formDataLength];
                                                int byteRead = 0;
                                                int totalBytesRead = 0;

                                                while (totalBytesRead < formDataLength) {
                                                    byteRead = in.read(dataBytes, totalBytesRead, formDataLength);
                                                    totalBytesRead += byteRead;
                                                }
                                                String file = new String(dataBytes);

                                                saveFile = file.substring(file.indexOf("filename=\"") + 10);
                                                saveFile = saveFile.substring(0, saveFile.indexOf("\n"));
                                                saveFile = saveFile.substring(saveFile.lastIndexOf("\\") + 1, saveFile.indexOf("\""));

                                                int lastIndex = contentType.lastIndexOf("=");

                                                String boundary = contentType.substring(lastIndex + 1, contentType.length());

                                                int pos;

                                                pos = file.indexOf("filename=\"");
                                                pos = file.indexOf("\n", pos) + 1;
                                                pos = file.indexOf("\n", pos) + 1;
                                                pos = file.indexOf("\n", pos) + 1;

                                                int boundaryLocation = file.indexOf(boundary, pos) - 4;

                                                int startPos = ((file.substring(0, pos)).getBytes()).length;
                                                int endPos = ((file.substring(0, boundaryLocation)).getBytes()).length;

                                                

                                                saveFile = "C:\\Users\\md_al\\Documents\\NetBeansProjects\\Kujaza\\web\\assets\\img\\users\\"+  id + ".jpg";
                                                saveFile2 = "C:\\Users\\md_al\\Documents\\NetBeansProjects\\Kujaza\\web\\assets\\img\\users\\"+  id + ".jpg";
                                                //saveFile = "C:/uploadDir2/" + saveFile;
                                                //out.print(saveFile);
                                                File ff = new File(saveFile);
                                                File ff2 = new File(saveFile2);

                                                try {
                                                    FileOutputStream fileOut = new FileOutputStream(ff);
                                                    fileOut.write(dataBytes, startPos, (endPos - startPos));
                                                    fileOut.flush();
                                                    fileOut.close();
                                                    FileOutputStream fileOut2 = new FileOutputStream(ff2);
                                                    fileOut2.write(dataBytes, startPos, (endPos - startPos));
                                                    fileOut2.flush();
                                                    fileOut2.close();

                                                } catch (Exception e) {
                                                    out.println(e);
                                                }

                                            }
                                            Thread.sleep(10000);
                                        %>
                                           
                                                                             <td><input type="file" name="file" value="" /></td>
                                </tr>
                                        <tr>
                                            <td></td>
                                                                             <td><input type="submit" value="UPLOAD" name="submit" /></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                        <td><img src="assets/img/users/<%=id%>.jpg" width="150px;" height="100px;" class="img-responsive img-circle center-block" /></td>
                                        </tr>
                                    </form>
                                
                                
                                
                                
                                <form action="settings.jsp" method="post">
                                
                                <tr>
                                    <td><%=tr[lang][160]%></td>
                                    <td><input name="name" class="form-control" type="text" value="<%=name%>"/></td>
                                </tr>
                                <tr>
                                    <td><%=tr[lang][161]%></td>
                                    <td><input name="email" type="email" class="form-control" value="<%=email%>" /></td>
                                </tr>
                                <tr>
                                    <td><%=tr[lang][162]%></td>
                                    <td><input type="textarea" name="description" class="aksTA form-control" maxlength="500" value="<%=description%>"></textarea></td>
                                </tr>
                                
                            </table>
                            <center>
                                <button type="submit" id="btnSaveChanges" class="aksBtn this"><%=tr[lang][187]%><span class="fa fa-save"></span></button>
                            </center>
                            </form>
                        </div>
                    </div>
                </div>
                
            </div>

            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingTwo">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            <i class="more-less fa fa-plus"></i>
                            <%=tr[lang][163]%>
                        </a>
                    </h4>
                </div>
                <form method="post" action="settings.jsp">
                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                    <div class="panel-body">
                        <div class="container-fluid">
                            <table class="table-responsive aksTbl">
                                <tr>
                                    <td><%=tr[lang][164]%></td>
                                    <td><input type="password" name="password" class="form-control" id="txtCurrPass" required="true"/></td>
                                </tr>
                                <tr>
                                    <td><%=tr[lang][165]%></td>
                                    <td><input type="password" name="newPassword" class="form-control" id="txtNewPass" required="true"/></td>
                                </tr>
                                <tr>
                                    <td><%=tr[lang][166]%></td>
                                    <td><input type="password" name="newPassword2" class="form-control" id="txtRetypeNewPass" required="true"/></td>
                                </tr>
                            </table>
                            <center>
                                <button type="submit" id="btnChangePass" class="aksBtn this"><%=tr[lang][167]%> <span class="fa fa-check"></span></button>
                            </center>
                        </div>
                    </div>
                </div>
                </form>
            </div>

            <%--<div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingThree">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            <i class="more-less fa fa-plus"></i>
                            Collapsible Group Item #3
                        </a>
                    </h4>
                </div>
                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                    <div class="panel-body">
                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                    </div>
                </div>
            </div>--%>

        </div><!-- panel-group -->
        </div>



        <!-- Modals -->
    <div id="SearchForm" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"><%=tr[lang][168]%> <span class="fa fa-search"></span></h4>
          </div>
          <div class="modal-body">
            <h2><%=tr[lang][169]%></h2>
            <div class="stepwizard col-md-offset-3">
                <div class="stepwizard-row setup-panel">
                  <div class="stepwizard-step">
                    <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
                    <p><%=tr[lang][170]%></p>
                  </div>
                  <div class="stepwizard-step">
                    <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                    <p><%=tr[lang][171]%></p>
                  </div>
                  <div class="stepwizard-step">
                    <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                    <p><%=tr[lang][172]%></p>
                  </div>
                </div>
              </div>
  
                <div class="row setup-content" id="step-1">
                      <span class="input input--kozakura">
					<input class="input__field input__field--kozakura" type="text" id="input-7" />
					<label class="input__label input__label--kozakura" for="input-7">
						<span class="input__label-content input__label-content--kozakura" data-content="Departure from"><%=tr[lang][173]%></span>
					</label>
					<svg class="graphic graphic--kozakura" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
						<path d="M1200,9c0,0-305.005,0-401.001,0C733,9,675.327,4.969,598,4.969C514.994,4.969,449.336,9,400.333,9C299.666,9,0,9,0,9v43c0,0,299.666,0,400.333,0c49.002,0,114.66,3.484,197.667,3.484c77.327,0,135-3.484,200.999-3.484C894.995,52,1200,52,1200,52V9z"/>
					</svg>
				</span>

            <span class="input input--kozakura">
					<input class="input__field input__field--kozakura" type="text" id="input-7" />
					<label class="input__label input__label--kozakura" for="input-7">
						<span class="input__label-content input__label-content--kozakura" data-content="Arrival to"><%=tr[lang][174]%></span>
					</label>
					<svg class="graphic graphic--kozakura" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
						<path d="M1200,9c0,0-305.005,0-401.001,0C733,9,675.327,4.969,598,4.969C514.994,4.969,449.336,9,400.333,9C299.666,9,0,9,0,9v43c0,0,299.666,0,400.333,0c49.002,0,114.66,3.484,197.667,3.484c77.327,0,135-3.484,200.999-3.484C894.995,52,1200,52,1200,52V9z"/>
					</svg>
				</span>
                      <button class="aksBtn nextBtn" type="button" ><span class="fa fa-arrow-right"></span></button>
                </div>
                <div class="row setup-content" id="step-2">
                      <select name="" id="cusSelectbox">
			            <option value="Cubic"><%=tr[lang][175]%></option>
			            <option value="Cylindrical"><%=tr[lang][176]%></option>
			            <option value="Car"><%=tr[lang][177]%></option>
		            </select>

            <span class="input input--kozakura">
					<input class="input__field input__field--kozakura" type="text" id="input-7" />
					<label class="input__label input__label--kozakura" for="input-7">
						<span class="input__label-content input__label-content--kozakura" data-content="Size"><%=tr[lang][178]%></span>
					</label>
					<svg class="graphic graphic--kozakura" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
						<path d="M1200,9c0,0-305.005,0-401.001,0C733,9,675.327,4.969,598,4.969C514.994,4.969,449.336,9,400.333,9C299.666,9,0,9,0,9v43c0,0,299.666,0,400.333,0c49.002,0,114.66,3.484,197.667,3.484c77.327,0,135-3.484,200.999-3.484C894.995,52,1200,52,1200,52V9z"/>
					</svg>
				</span>
                      <button class="aksBtn nextBtn" type="button" ><span class="fa fa-arrow-right"></span></button>
                </div>
                <div class="row setup-content" id="step-3">
                  <div class="col-xs-6 col-md-offset-3">
                    <div class="col-md-12">
                        <!--<span class="input input--kozakura">
					        <input id="theInput" name="datetimepicker" class="input__field input__field--kozakura" type="text" id="input-7" />
					        <label class="input__label input__label--kozakura" for="input-7">
						        <span class="input__label-content input__label-content--kozakura" data-content="Ship Date">Ship Date</span>
					        </label>
					        <svg class="graphic graphic--kozakura" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
						        <path d="M1200,9c0,0-305.005,0-401.001,0C733,9,675.327,4.969,598,4.969C514.994,4.969,449.336,9,400.333,9C299.666,9,0,9,0,9v43c0,0,299.666,0,400.333,0c49.002,0,114.66,3.484,197.667,3.484c77.327,0,135-3.484,200.999-3.484C894.995,52,1200,52,1200,52V9z"/>
					        </svg>
				        </span>-->
                        <input type="text" placeholder="Ship Date" id="theInput" />
                      <button class="aksBtn" type="submit"><i class="fa fa-search"></i></button>
                    </div>
                </div>
        </div>
          </div>
        </div>
          </div>
      </div>


         <!-- Scripts -->
    <script src="assets/js/jquery-3.1.1.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/classie.js"></script>
    <script src="assets/js/KeepOpen.js"></script>
    <script src="assets/js/typed.min.js"></script>
    <script src="assets/js/aos.js"></script>
    <script>AOS.init();</script>
    <script src="assets/js/main.js"></script>
    <script src="assets/js/DatePicker.js"></script>
        <script src="assets/js/cbpFWTabs.js"></script>
        <script>
			(function() {

				[].slice.call( document.querySelectorAll( '.tabs' ) ).forEach( function( el ) {
					new CBPFWTabs( el );
				});

			})();

			$('.acc-type').click(function () {
			    $('.acc-type').removeClass('this-type');
			    $('.acc-type-chkd').removeAttr('checked');
			    $(this).addClass('this-type');
			    $(this).next('.acc-type-chkd').attr('checked', 'checked');
			});

            $(document).ready(function(){
                $('[data-toggle="tooltip"]').tooltip();   
            });

            function toggleIcon(e) {
                $(e.target)
                    .prev('.panel-heading')
                    .find(".more-less")
                    .toggleClass('fa-plus fa-minus');
            }
            $('.panel-group').on('hidden.bs.collapse', toggleIcon);
            $('.panel-group').on('shown.bs.collapse', toggleIcon);
		</script>
    </div>
            
            <%
//        } catch (Exception e) {
//                System.out.println(e);
//            } finally {
//                db.close();
//            }
        %>
</body>
</html>
